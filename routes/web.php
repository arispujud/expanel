<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/
Route::group(array('before' => 'auth'), function(){
    Route::get('/', 'HomeController@index');

    Route::get('/pelatihantanpaalat', 'HomeController@pelatihanTanpaAlat');
    Route::post('/pelatihantanpaalat', 'ExportController@savePelatihanTanpaAlat');

    Route::get('/monev','HomeController@monev');
    Route::post('/monev', 'ExportController@saveMonev');

    Route::get('/pelatihandenganalat','HomeController@pelatihanDenganAlat');
    Route::post('/pelatihandenganalat','ExportController@savePelatihanDenganAlat');

    Route::get('/pameran','HomeController@pameran');
    Route::post('/pameran','ExportController@savePameran');

    Route::get('/pameranmulti','HomeController@pameranMulti');
    Route::post('/pameranmulti','ExportController@savePameranMulti');

    Route::get('/fasilitasi','HomeController@fasilitasi');
    Route::post('/fasilitasi','ExportController@saveFasilitasi');

    Route::get('/export','ExportController@index');

    Route::get('/manageuser','HomeController@manageUser');
    Route::post('/manageuser/add','UserController@addUser');
    Route::get('/manageuser/delete/{key?}','UserController@deleteUser');
    Route::get('/manageuser/edit/{key?}','HomeController@editUser');
    Route::post('/manageuser/update','UserController@updateUser');

});
Route::get('/permission', function(){
        return view('errors.permission')->with('data','Hak Akses');
});
//Route::get('/', 'HomeController@index');
// Route::get('/pelatihantanpaalat', function () {
//     return view('home.page')->with('data','Pelatihan Tanpa Alat');
// });

// Route::get('/pelatihandenganalat', function () {
    
// });
// Route::get('/pameranmulti', function () {
//     return view('home.page')->with('data','Pameran Multi');
// });
// Route::get('/pameran', function () {
//     return view('home.page')->with('data','Pameran');
// });
// Route::get('/fasilitasi', function () {
//     return view('home.page')->with('data','Fasilitasi');
// });


Route::get('/login', function(){
	return view('login.page')->with('msg',null);
});
Route::post('/login', 'UserController@login');
Route::get('/logout', 'UserController@logout');
Route::get('/pass', function(){
	return Hash::make('admin');
});
Route::get('/coba', function(){
   return view('form.monev')->with('data','XXXXX');
});


