<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

use App\Http\Requests;
use App\Models\UserModel;
use Hash;

class UserController extends Controller
{
    public function login(Request $request){
    	$input = $request->all();
    	$getUser = UserModel::select('username','nama', 'role', 'token')
    					->where('username',$input['username'])
    					->orWhere('email',$input['username'])
    					->first();
    	$authByUsername = Auth::attempt(['username' => $input['username'], 'password' => $input['password']]);
    	$authByEmail = Auth::attempt(['email' => $input['username'], 'password' => $input['password']]);

    	if(($authByUsername==true)||($authByEmail==true)){
    		$session = ['username'  => $getUser['username'],
    					'nama'		=> $getUser['nama'],
                        'role'      => $getUser['role'],
                        'token'     => $getUser['token']];
    		Session::set('expanel',$session);
			return redirect('/');
    	}
    	else{
    		$session = null;
            return redirect('/login')->with('err', 'Username & Password Salah');
    	}
    	return $session;
    }

    public function logout(){
    	$sesion = Session::flush();
		return redirect('/login');
    }

    public function addUser(Request $request){
        $session = Session::get('expanel');
        if($session['role']!='admin'){
            return redirect('permission');
        }
        $getRequest = $request->all();
        $status = false;
        $error_msg = null;
        if(isset($getRequest['fullname']) &&
            isset($getRequest['username']) && 
            isset($getRequest['email']) &&
            isset($getRequest['pass']) &&
            isset($getRequest['pass_con']) &&
            isset($getRequest['permission']) &&
            isset($getRequest['_token']))
        {
            if($getRequest['pass'] != $getRequest['pass_con']){
                $error_msg = "Konfirmasi Password Salah!"; 
            }
            else{
                $getUsername = UserModel::where('username', $getRequest['username'])->get();
                if(count($getUsername)>0){
                    $error_msg = "Username sudah terdaftar!";
                }else{
                    $getEmail = UserModel::where('email', $getRequest['email'])->get();
                    if(count($getEmail)>0){
                        $error_msg = "Email sudah terdaftar!";
                    }
                    else{
                        $status = true;
                        $error_msg = "User berhasil ditambahkan!";
                        $data = ['nama'     => $getRequest['fullname'],
                                 'username' => $getRequest['username'],
                                 'email'    => $getRequest['email'],
                                 'password' => Hash::make($getRequest['pass']),
                                 'role'     => $getRequest['permission']];
                        UserModel::insert($data);
                    }
                }
            }

        }
        if($status==true){
            $dataRequest = null;
        }
        else{
            $dataRequest = $getRequest;
        }
        $res = ['status' => $status,
                'msg'    => $error_msg,
                'input'  => $dataRequest];
        // return ["status" => $status,
        //         "error"   => $error_msg];
        return redirect('manageuser')->with('set_data', $res);
    }

    public function deleteUser($key=null){
        $session = Session::get('expanel');
        if($session['role']!='admin'){
            return redirect('permission');
        }
        $status = false;
        $error_msg = null;
        if($key!=null){
            UserModel::where('id', $key)
                    ->delete();
            $status = true;
            $error_msg = "Data telah dihapus!";
        }
        else{
            $error_msg = "Tidak ada data yang dihapus!";
        }

        $res = ['status' => $status,
                'msg'    => $error_msg];
        return redirect('manageuser')->with('set_data', $res);
    }

    public function updateUser(Request $request){
        $session = Session::get('expanel');
        if($session['role']!='admin'){
            return redirect('permission');
        }
        $getRequest = $request->all();
        $status = false;
        $error_msg = null;
        
        $data = ['nama'     => $getRequest['fullname'],
                 'username' => $getRequest['username'],
                 'email'    => $getRequest['email'],
                 'role'     => $getRequest['permission']];
        if(isset($getRequest['edit_password'])){
            if($getRequest['pass'] != $getRequest['pass_con']){
                $error_msg = "Konfirmasi Password Salah!"; 
            }
            else{
                $data['password'] = Hash::make($getRequest['pass']);
            }
        }

        UserModel::where('id', $getRequest['id'])
                    ->update($data);
        $status = true;
        $error_msg = "Data telah diupdate!";
        

        $res = ['status' => $status,
                'msg'    => $error_msg];
        return redirect('manageuser')->with('set_data', $res);
    }
}
