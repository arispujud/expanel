<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
//use Illuminate\Config\Repository;
//use Illuminate\Support\Facades\Facade;

use App\Http\Requests;
use Excel;
class ExportController extends Controller{
	public function __construct()
	{
	  $this->middleware('auth');
	}
	public function getNamaBulan($bulan){
		switch ($bulan) {
			case '1':
				$strBulan = 'Januari';
				break;
			case '2':
				$strBulan = 'Februari';
				break;
			case '3':
				$strBulan = 'Maret';
				break;
			case '4':
				$strBulan = 'April';
				break;
			case '5':
				$strBulan = 'Mei';
				break;
			case '6':
				$strBulan = 'Juni';
				break;
			case '7':
				$strBulan = 'Juli';
				break;
			case '8':
				$strBulan = 'Agustus';
				break;
			case '9':
				$strBulan = 'September';
				break;
			case '10':
				$strBulan = 'Oktober';
				break;
			case '11':
				$strBulan = 'November';
				break;
			case '12':
				$strBulan = 'Desember';
				break;										
			default:
				$strBulan = null;
				break;
		}
		return $strBulan;
	}
	public function index(){
		$pathFile = "public/templateExcel/pameran.xls";
	//	dd($pathFile); exit();
		ini_set('max_execution_time', 300);
	    Excel::load($pathFile, function($file) {

	    	// $sheet = $file->setActiveSheetIndex(0);
	    	// $sheet->setCellValue('C4', 2000);
	    	// $sheet->setCellValue('D4', 2);

		})->export('xls');
		return true;
	}
	public function saveMonev(Request $request){
		$getInput = $request->all();
		$strBulan = ExportController::getNamaBulan(date('m'));
		
		$tglSekarang = date('d')." ".$strBulan." ".date('Y');
		$pathFile = "public/templateExcel/RKA_Monev.xls";
		ini_set('max_execution_time', 300);
	    Excel::load($pathFile, function($file) use ($getInput,$tglSekarang) {

	    	$sheet = $file->setActiveSheetIndex(0);
	    	$sheet->setCellValue('T3', $getInput['formulir']);
	    	$sheet->setCellValue('A4', "Tahun Anggaran ".$getInput['thn_anggaran']);
	    	$sheet->setCellValue('R31', $getInput['vol_atk']);
	    	$sheet->setCellValue('G9', $getInput['lokasi_kegiatan']);
	    	$sheet->setCellValue('H20', $getInput['sasaran']);
	    	$sheet->setCellValue('F15', $getInput['capaian']);
	    	$sheet->setCellValue('F16', $getInput['masukkan']);
	    	$sheet->setCellValue('F17', $getInput['keluaran']);
	    	$sheet->setCellValue('F19', $getInput['hasil']);

	    	$sheet->setCellValue('R31', $getInput['vol_atk']);
	    	$sheet->setCellValue('R35', $getInput['vol_pengadaan']);
	    	$sheet->setCellValue('R39', $getInput['vol_mami']);
	    	$sheet->setCellValue('R44', $getInput['vol_Gol4']);
	    	$sheet->setCellValue('R45', $getInput['vol_Gol3']);
	    	$sheet->setCellValue('R46', $getInput['vol_Gol3']);

	    	$sheet->setCellValue('H65', $getInput['keterangan']);
	    	$sheet->setCellValue('H66', $getInput['tgl_pembahasan']);
	    	$sheet->setCellValue('B68', $getInput['catatan_pembahasan1']);
	    	$sheet->setCellValue('B69', $getInput['catatan_pembahasan2']);
	    	$sheet->setCellValue('B70', $getInput['catatan_pembahasan3']);

			$sheet->setCellValue('C73', $getInput['tim_anggaran1']);
			$sheet->setCellValue('I73', $getInput['nim_anggaran1']);
			$sheet->setCellValue('C74', $getInput['tim_anggaran2']);
			$sheet->setCellValue('I74', $getInput['nim_anggaran2']);
			$sheet->setCellValue('C75', $getInput['tim_anggaran3']);
			$sheet->setCellValue('I75', $getInput['nim_anggaran3']);

			$sheet->setCellValue('T51', "Bantul, ".$tglSekarang);
		})->export('xls');
		return view('form.monev')->with('data','Monev');
	}

	public function savePelatihanDenganAlat(Request $request){
		$getInput = $request->all();
		$strBulan = ExportController::getNamaBulan(date('m'));
		
		$tglSekarang = date('d')." ".$strBulan." ".date('Y');
		$pathFile = "public/templateExcel/pelatihan_dengan_alat_dan_praktek_lapangan.xls";
		ini_set('max_execution_time', 300);
	    Excel::load($pathFile, function($file) use ($getInput, $tglSekarang) {

	    	$sheet = $file->setActiveSheetIndex(0);
	    	$sheet->setCellValue('A4', "Tahun Anggaran ".$getInput['thn_anggaran']);
	    	$sheet->setCellValue('T3', $getInput['formulir']);
	    	$sheet->setCellValue('F9', $getInput['lokasi_kegiatan']);

	    	$sheet->setCellValue('F15', $getInput['capaian']);
	    	$sheet->setCellValue('F16', $getInput['masukkan']);
	    	$sheet->setCellValue('F17', $getInput['keluaran']);

	    	$sheet->setCellValue('F18', $getInput['hasil']);
	    	$sheet->setCellValue('H20', $getInput['sasaran']);
	    	$sheet->setCellValue('R28', $getInput['vol_honor_tim1']);
	    	$sheet->setCellValue('R29', $getInput['vol_honor_tim2']);
	    	$sheet->setCellValue('R30', $getInput['vol_honor_tim3']);

	    	$sheet->setCellValue('R35', $getInput['vol_atk1']);
	    	$sheet->setCellValue('R36', $getInput['vol_atk2']);
	    	$sheet->setCellValue('R37', $getInput['vol_atk3']);
	    	$sheet->setCellValue('R40', $getInput['vol_bm1']);
	    	$sheet->setCellValue('R41', $getInput['vol_bm2']);
	    	$sheet->setCellValue('R44', $getInput['vol_bm3']);
	    	$sheet->setCellValue('R56', $getInput['vol_jk1']);
	    	$sheet->setCellValue('R58', $getInput['vol_jk2']);
	    	$sheet->setCellValue('R62', $getInput['vol_cp1']);
	    	$sheet->setCellValue('R67', $getInput['vol_stp1']);
	    	$sheet->setCellValue('R70', $getInput['vol_sp1']);
	    	$sheet->setCellValue('R73', $getInput['vol_ss1']);
	    	$sheet->setCellValue('R74', $getInput['vol_ss2']);
	    	$sheet->setCellValue('R78', $getInput['vol_mm1']);
	    	$sheet->setCellValue('R81', $getInput['vol_mm2']);
	    	$sheet->setCellValue('R82', $getInput['vol_mm3']);
	    	$sheet->setCellValue('R86', $getInput['vol_mm4']);
	    	$sheet->setCellValue('R87', $getInput['vol_mm5']);
	    	$sheet->setCellValue('R93', $getInput['vol_pdd1']);
	    	$sheet->setCellValue('R94', $getInput['vol_pdd2']);
	    	$sheet->setCellValue('R97', $getInput['vol_pdd3']);
	    	$sheet->setCellValue('R98', $getInput['vol_pdd4']);
	    	$sheet->setCellValue('R102', $getInput['vol_pld1']);
	    	$sheet->setCellValue('R104', $getInput['vol_pld2']);
	    	$sheet->setCellValue('R110', $getInput['vol_honor_pk1']);
	    	$sheet->setCellValue('R111', $getInput['vol_honor_pk2']);
	    	$sheet->setCellValue('R125', $getInput['vol_honor_pk3']);
	    	$sheet->setCellValue('R130', "Bantul, ".$tglSekarang);
	    	$sheet->setCellValue('H137', $getInput['keterangan']);
	    	$sheet->setCellValue('H138', $getInput['tgl_pembahasan']);
	    	$sheet->setCellValue('B140', $getInput['catatan_pembahasan1']);
	    	$sheet->setCellValue('B141', $getInput['catatan_pembahasan2']);
	    	$sheet->setCellValue('B142', $getInput['catatan_pembahasan3']);
	    	$sheet->setCellValue('C145', $getInput['tim_anggaran1']);
	    	$sheet->setCellValue('C146', $getInput['tim_anggaran2']);
	    	$sheet->setCellValue('C147', $getInput['tim_anggaran3']);
	    	$sheet->setCellValue('I145', $getInput['nim_anggaran1']);
	    	$sheet->setCellValue('I146', $getInput['nim_anggaran2']);
	    	$sheet->setCellValue('I147', $getInput['nim_anggaran3']);
		})->export('xls');
		return view('form.monev')->with('data','Monev');
	}

	public function savePelatihanTanpaAlat(Request $request){
		$getInput = $request->all();
		$strBulan = ExportController::getNamaBulan(date('m'));
		
		$tglSekarang = date('d')." ".$strBulan." ".date('Y');
		$pathFile = "public/templateExcel/RKA_Pelatihan_tanpa_alat.xls";
		ini_set('max_execution_time', 300);
	    Excel::load($pathFile, function($file) use ($getInput, $tglSekarang) {

	    	$sheet = $file->setActiveSheetIndex(0);
	    	$sheet->setCellValue('A4', "Tahun Anggaran ".$getInput['thn_anggaran']);
	    	$sheet->setCellValue('T3', $getInput['formulir']);
	    	$sheet->setCellValue('H9', $getInput['lokasi_kegiatan']);

	    	$sheet->setCellValue('H15', $getInput['capaian']);
	    	$sheet->setCellValue('H16', $getInput['masukkan']);
	    	$sheet->setCellValue('H17', $getInput['keluaran']);

	    	$sheet->setCellValue('H18', $getInput['hasil']);
	    	$sheet->setCellValue('I19', $getInput['sasaran']);
	    	
	    	$sheet->setCellValue('R31', $getInput['vol_atk']);
	    	$sheet->setCellValue('R32', $getInput['vol_bap']);

	    	$sheet->setCellValue('R36', $getInput['vol_bbp']);

	    	$sheet->setCellValue('R39', $getInput['vol_bd']);

	    	$sheet->setCellValue('R43', $getInput['vol_bcp']);

	    	$sheet->setCellValue('R47', $getInput['vol_stk']);

	    	$sheet->setCellValue('R51', $getInput['vol_bmk']);
	    	$sheet->setCellValue('R52', $getInput['vol_mdm']);

	    	$sheet->setCellValue('R65', $getInput['vol_gol2']);
	    	$sheet->setCellValue('R66', $getInput['vol_gol3']);
	    	$sheet->setCellValue('R67', $getInput['vol_gol4']);
	    	$sheet->setCellValue('R71', $getInput['vol_hr']);
	    	$sheet->setCellValue('R74', $getInput['vol_hps']);

	    	$sheet->setCellValue('J86', $getInput['keterangan']);
	    	$sheet->setCellValue('J87', $getInput['tgl_pembahasan']);

	    	$sheet->setCellValue('B89', $getInput['catatan_pembahasan1']);
	    	$sheet->setCellValue('B90', $getInput['catatan_pembahasan2']);
	    	$sheet->setCellValue('B91', $getInput['catatan_pembahasan3']);

	    	$sheet->setCellValue('C94', $getInput['tim_anggaran1']);
	    	$sheet->setCellValue('C95', $getInput['tim_anggaran2']);
	    	$sheet->setCellValue('C96', $getInput['tim_anggaran3']);
	    	$sheet->setCellValue('K94', $getInput['nim_anggaran1']);
	    	$sheet->setCellValue('K95', $getInput['nim_anggaran2']);
	    	$sheet->setCellValue('K96', $getInput['nim_anggaran3']);

	    	$sheet->setCellValue('T78', "Bantul, ".$tglSekarang);
	    	
		})->export('xls');
		return view('form.pelatihantanpaalat')->with('data','Pelatihan Tanpa Alat');
	}

	public function saveFasilitasi(Request $request){
		$getInput = $request->all();
		$strBulan = ExportController::getNamaBulan(date('m'));
		
		$tglSekarang = date('d')." ".$strBulan." ".date('Y');
		$pathFile = "public/templateExcel/fasilitasi_atau_sosialisasi_workshop.xls";
		ini_set('max_execution_time', 300);
	    Excel::load($pathFile, function($file) use ($getInput, $tglSekarang) {

	    	$sheet = $file->setActiveSheetIndex(0);
	    	$sheet->setCellValue('B4', "Tahun Anggaran ".$getInput['thn_anggaran']);
	    	$sheet->setCellValue('Q3', $getInput['formulir']);
	    	$sheet->setCellValue('G9', $getInput['lokasi_kegiatan']);

	    	$sheet->setCellValue('G15', $getInput['capaian']);
	    	$sheet->setCellValue('G16', $getInput['masukkan']);
	    	$sheet->setCellValue('G17', $getInput['keluaran']);

	    	$sheet->setCellValue('G18', $getInput['hasil']);
	    	$sheet->setCellValue('J19', $getInput['sasaran']);

	    	$sheet->setCellValue('O30', $getInput['vol_atk']);
	    	$sheet->setCellValue('O31', $getInput['vol_peserta']);
	    	$sheet->setCellValue('O34', $getInput['vol_bd']);

	    	$sheet->setCellValue('O37', $getInput['vol_bp']);

	    	$sheet->setCellValue('O41', $getInput['vol_sewa']);

	    	$sheet->setCellValue('O44', $getInput['vol_bmr']);
	    	$sheet->setCellValue('O46', $getInput['vol_bmk']);

	    	$sheet->setCellValue('O50', $getInput['vol_hm']);
	    	$sheet->setCellValue('O51', $getInput['vol_hn']);

	    	$sheet->setCellValue('O75', $getInput['vol_pes']);
	    	

	    	$sheet->setCellValue('P79', "Bantul, ".$tglSekarang);
	    	
	    	$sheet->setCellValue('H88', $getInput['keterangan']);
	    	$sheet->setCellValue('H89', $getInput['tgl_pembahasan']);

	    	$sheet->setCellValue('D91', $getInput['catatan_pembahasan1']);
	    	$sheet->setCellValue('D92', $getInput['catatan_pembahasan2']);
	    	$sheet->setCellValue('D93', $getInput['catatan_pembahasan3']);

	    	$sheet->setCellValue('D96', $getInput['tim_anggaran1']);
	    	$sheet->setCellValue('D97', $getInput['tim_anggaran2']);
	    	$sheet->setCellValue('D98', $getInput['tim_anggaran3']);

	    	$sheet->setCellValue('L96', $getInput['nim_anggaran1']);
	    	$sheet->setCellValue('L97', $getInput['nim_anggaran2']);
	    	$sheet->setCellValue('L98', $getInput['nim_anggaran3']);

		})->export('xls');
		return view('form.fasilitasi')->with('data','Fasilitasi');
	}

	public function savePameran(Request $request){
		$getInput = $request->all();
		$strBulan = ExportController::getNamaBulan(date('m'));
		
		$tglSekarang = date('d')." ".$strBulan." ".date('Y');
		$pathFile = "public/templateExcel/pameran.xls";
		ini_set('max_execution_time', 300);
	    Excel::load($pathFile, function($file) use ($getInput, $tglSekarang) {

	    	$sheet = $file->setActiveSheetIndex(0);
	    	$sheet->setCellValue('A4', "Tahun Anggaran ".$getInput['thn_anggaran']);
	    	$sheet->setCellValue('T3', $getInput['formulir']);
	    	$sheet->setCellValue('A3', $getInput['lokasi_kegiatan']);

	    	$sheet->setCellValue('F15', $getInput['capaian']);
	    	$sheet->setCellValue('F16', $getInput['masukkan']);
	    	$sheet->setCellValue('F17', $getInput['keluaran']);

	    	$sheet->setCellValue('F18', $getInput['hasil']);
	    	$sheet->setCellValue('K19', $getInput['sasaran']);

	    	$sheet->setCellValue('R28', $getInput['vol_hppk']);
	    	$sheet->setCellValue('R29', $getInput['vol_hpp']);
	    	$sheet->setCellValue('R30', $getInput['vol_hasil']);

	    	$sheet->setCellValue('R35', $getInput['vol_bsk']);
	    	$sheet->setCellValue('R36', $getInput['vol_bat']);

	    	$sheet->setCellValue('R39', $getInput['vol_svr']);

	    	$sheet->setCellValue('R44', $getInput['vol_dks']);

	    	$sheet->setCellValue('R48', $getInput['vol_bcl']);
	    	$sheet->setCellValue('R49', $getInput['vol_bcb']);

	    	$sheet->setCellValue('R52', $getInput['vol_bfc']);

	    	$sheet->setCellValue('R55', $getInput['vol_bsm']);

	    	$sheet->setCellValue('R58', $getInput['vol_bsp']);

	    	$sheet->setCellValue('R61', $getInput['vol_makan']);

	    	$sheet->setCellValue('R66', $getInput['vol_lps']);
	    	$sheet->setCellValue('R67', $getInput['vol_tpt']);
	    	$sheet->setCellValue('R68', $getInput['vol_lmm']);
	    	$sheet->setCellValue('R69', $getInput['vol_trt']);

	    	$sheet->setCellValue('R78', $getInput['vol_lum']);
	    	$sheet->setCellValue('R79', $getInput['vol_tra']);
	    	$sheet->setCellValue('R80', $getInput['vol_lms']);
	    	$sheet->setCellValue('R81', $getInput['vol_prt']);

	    	$sheet->setCellValue('R86', $getInput['vol_usp']);
	    	

	    	$sheet->setCellValue('R91', "Bantul, ".$tglSekarang);
	    	
	    	$sheet->setCellValue('H99', $getInput['keterangan']);
	    	$sheet->setCellValue('H100', $getInput['tgl_pembahasan']);

	    	$sheet->setCellValue('B102', $getInput['catatan_pembahasan1']);
	    	$sheet->setCellValue('B103', $getInput['catatan_pembahasan2']);
	    	$sheet->setCellValue('B104', $getInput['catatan_pembahasan3']);

	    	$sheet->setCellValue('C107', $getInput['tim_anggaran1']);
	    	$sheet->setCellValue('C108', $getInput['tim_anggaran2']);
	    	$sheet->setCellValue('C109', $getInput['tim_anggaran3']);

	    	$sheet->setCellValue('I107', $getInput['nim_anggaran1']);
	    	$sheet->setCellValue('I108', $getInput['nim_anggaran2']);
	    	$sheet->setCellValue('I109', $getInput['nim_anggaran3']);

		})->export('xls');
		return view('form.pameran')->with('data','Pameran');
	}

	public function savePameranMulti(Request $request){
		$getInput = $request->all();
		$strBulan = ExportController::getNamaBulan(date('m'));
		
		$tglSekarang = date('d')." ".$strBulan." ".date('Y');
		$pathFile = "public/templateExcel/pameran multi.xls";
		ini_set('max_execution_time', 300);
	    Excel::load($pathFile, function($file) use ($getInput, $tglSekarang) {

	    	$sheet = $file->setActiveSheetIndex(0);
	    	$sheet->setCellValue('A5', "Tahun Anggaran ".$getInput['thn_anggaran']);
	    	$sheet->setCellValue('K4', $getInput['formulir']);
	    	$sheet->setCellValue('A4', $getInput['lokasi_kegiatan']);

	    	$sheet->setCellValue('F16', $getInput['capaian']);
	    	$sheet->setCellValue('F17', $getInput['masukkan']);
	    	$sheet->setCellValue('F18', $getInput['keluaran']);

	    	$sheet->setCellValue('F19', $getInput['hasil']);
	    	$sheet->setCellValue('F20', $getInput['sasaran']);

	    	$sheet->setCellValue('I31', $getInput['vol_tim']);
	    	$sheet->setCellValue('I32', $getInput['vol_hnr']);

	    	$sheet->setCellValue('I37', $getInput['vol_pmr']);
	    	$sheet->setCellValue('I38', $getInput['vol_icf']);
	    	$sheet->setCellValue('I39', $getInput['vol_jkt']);
	    	$sheet->setCellValue('I40', $getInput['vol_tei']);
	    	$sheet->setCellValue('I41', $getInput['vol_afm']);

	    	$sheet->setCellValue('I45', $getInput['vol_dmr']);
	    	$sheet->setCellValue('I46', $getInput['vol_dcf']);
	    	$sheet->setCellValue('I47', $getInput['vol_dkt']);
	    	$sheet->setCellValue('I48', $getInput['vol_dei']);
	    	$sheet->setCellValue('I49', $getInput['vol_dfm']);

	    	$sheet->setCellValue('I53', $getInput['vol_clm']);
	    	$sheet->setCellValue('I54', $getInput['vol_cln']);
	    	$sheet->setCellValue('I55', $getInput['vol_clp']);
	    	$sheet->setCellValue('I56', $getInput['vol_clq']);
	    	$sheet->setCellValue('I57', $getInput['vol_clr']);

	    	$sheet->setCellValue('I60', $getInput['vol_ska']);
	    	$sheet->setCellValue('I61', $getInput['vol_skb']);
	    	$sheet->setCellValue('I62', $getInput['vol_skc']);
	    	$sheet->setCellValue('I63', $getInput['vol_skd']);
	    	$sheet->setCellValue('I64', $getInput['vol_skf']);

	    	$sheet->setCellValue('I68', $getInput['vol_eka']);
	    	$sheet->setCellValue('I69', $getInput['vol_ekb']);
	    	$sheet->setCellValue('I70', $getInput['vol_ekc']);
	    	$sheet->setCellValue('I71', $getInput['vol_ekd']);
	    	$sheet->setCellValue('I72', $getInput['vol_ekf']);

	    	$sheet->setCellValue('I76', $getInput['vol_mka']);
	    	$sheet->setCellValue('I77', $getInput['vol_mkb']);
	    	$sheet->setCellValue('I78', $getInput['vol_mkc']);
	    	$sheet->setCellValue('I79', $getInput['vol_mkd']);
	    	$sheet->setCellValue('I80', $getInput['vol_mkf']);

	    	$sheet->setCellValue('I84', $getInput['vol_rka']);
	    	$sheet->setCellValue('I85', $getInput['vol_rkb']);
	    	$sheet->setCellValue('I86', $getInput['vol_rkc']);
	    	$sheet->setCellValue('I87', $getInput['vol_rkd']);
	    	$sheet->setCellValue('I88', $getInput['vol_rkf']);

	    	$sheet->setCellValue('I92', $getInput['vol_kka']);
	    	$sheet->setCellValue('I93', $getInput['vol_kkb']);
	    	$sheet->setCellValue('I94', $getInput['vol_kkc']);
	    	$sheet->setCellValue('I95', $getInput['vol_kkd']);
	    	$sheet->setCellValue('I96', $getInput['vol_kkf']);

	    	$sheet->setCellValue('I100', $getInput['vol_lka']);
	    	$sheet->setCellValue('I101', $getInput['vol_lkb']);
	    	$sheet->setCellValue('I102', $getInput['vol_lkc']);
	    	$sheet->setCellValue('I103', $getInput['vol_lkd']);
	    	$sheet->setCellValue('I104', $getInput['vol_lkf']);

	    	$sheet->setCellValue('I108', $getInput['vol_oka']);
	    	$sheet->setCellValue('I109', $getInput['vol_vka']);

	    	$sheet->setCellValue('I110', $getInput['vol_okb']);
	    	$sheet->setCellValue('I111', $getInput['vol_vkb']);

	    	$sheet->setCellValue('I112', $getInput['vol_okc']);
	    	$sheet->setCellValue('I113', $getInput['vol_vkc']);

	    	$sheet->setCellValue('I114', $getInput['vol_zkc']);

	    	$sheet->setCellValue('I116', $getInput['vol_wka']);
	    	$sheet->setCellValue('I117', $getInput['vol_ika']);

	    	$sheet->setCellValue('I118', $getInput['vol_wkb']);
	    	$sheet->setCellValue('I119', $getInput['vol_ikb']);

	    	$sheet->setCellValue('I120', $getInput['vol_wkc']);
	    	$sheet->setCellValue('I121', $getInput['vol_ikc']);

	    	$sheet->setCellValue('I122', $getInput['vol_jkc']);

	    	$sheet->setCellValue('I125', $getInput['vol_0ka']);
	    	$sheet->setCellValue('I126', $getInput['vol_xkx']);

	    	$sheet->setCellValue('I127', $getInput['vol_uka']);
	    	$sheet->setCellValue('I128', $getInput['vol_xka']);

	    	$sheet->setCellValue('I129', $getInput['vol_ukb']);
	    	$sheet->setCellValue('I130', $getInput['vol_xkb']);

	    	$sheet->setCellValue('I131', $getInput['vol_ukc']);
	    	$sheet->setCellValue('I132', $getInput['vol_xkc']);

	    	$sheet->setCellValue('I133', $getInput['vol_tkc']);

	    	$sheet->setCellValue('I135', $getInput['vol_hka']);
	    	$sheet->setCellValue('I136', $getInput['vol_nka']);

	    	$sheet->setCellValue('I137', $getInput['vol_hkb']);
	    	$sheet->setCellValue('I138', $getInput['vol_nkb']);

	    	$sheet->setCellValue('I139', $getInput['vol_hkc']);
	    	$sheet->setCellValue('I140', $getInput['vol_nkc']);

	    	$sheet->setCellValue('I141', $getInput['vol_1kc']);

	    	$sheet->setCellValue('I143', $getInput['vol_qqa']);
	    	$sheet->setCellValue('I144', $getInput['vol_xxa']);

	    	$sheet->setCellValue('I145', $getInput['vol_qqb']);
	    	$sheet->setCellValue('I146', $getInput['vol_xxb']);

	    	$sheet->setCellValue('I147', $getInput['vol_qqc']);
	    	$sheet->setCellValue('I148', $getInput['vol_xxc']);

	    	$sheet->setCellValue('I149', $getInput['vol_qqd']);
	    	$sheet->setCellValue('I150', $getInput['vol_xxd']);


	    	$sheet->setCellValue('J155', "Bantul, ".$tglSekarang);
	    	
	    	$sheet->setCellValue('F162', $getInput['keterangan']);
	    	$sheet->setCellValue('F163', $getInput['tgl_pembahasan']);

	    	$sheet->setCellValue('B165', $getInput['catatan_pembahasan1']);
	    	$sheet->setCellValue('B166', $getInput['catatan_pembahasan2']);
	    	$sheet->setCellValue('B167', $getInput['catatan_pembahasan3']);

	    	$sheet->setCellValue('C170', $getInput['tim_anggaran1']);
	    	$sheet->setCellValue('C171', $getInput['tim_anggaran2']);
	    	$sheet->setCellValue('C172', $getInput['tim_anggaran3']);

	    	$sheet->setCellValue('G170', $getInput['nim_anggaran1']);
	    	$sheet->setCellValue('G171', $getInput['nim_anggaran2']);
	    	$sheet->setCellValue('G172', $getInput['nim_anggaran3']);

		})->export('xls');
		return view('form.pameranmulti')->with('data','PameranMulti');
	}
}
