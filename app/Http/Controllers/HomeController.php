<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Models\UserModel;

class HomeController extends Controller
{
    public function __construct()
     {
          $this->middleware('auth');
          $this->user = new UserModel;
     }

    public function cekLogin(){
        $status  = false;
        $data    = null;
        $session = Session::get('expanel');
        if(isset($session['username'])){
            $status = true;
            $data   = $session;
        }
        $res = ['status' => $status,
                'data'   => $data];
        return $res;
    }
    public function index()
    {
        // $dataLogin = HomeController::cekLogin();
        // if($dataLogin['status']==false){
        //     return redirect('/login');
        // }

        //return $dataLogin;
        return view('home.page')->with('data','Dashboard');
    }

    public function pelatihanTanpaAlat(){
        return view('form.pelatihantanpaalat')->with('data','Pelatihan Tanpa Alat');
    }

    public function pelatihanDenganAlat(){
        return view('form.pelatihandenganalat')->with('data','Pelatihan Dengan Alat');
    }

    public function monev(){
        return view('form.monev')->with('data','Monev');
    }

    public function pameran(){
        return view('form.pameran')->with('data','Pameran');
    }

    public function pameranMulti(){
        return view('form.pameranmulti')->with('data','Pameran Multi');
    }

    public function fasilitasi(){
        return view('form.fasilitasi')->with('data','Fasilitasi');
    }

    public function manageUser(){
        $session = Session::get('expanel');
        if($session['role']!='admin'){
            return redirect('permission');
        }
        $getUser = UserModel::all();
        $getSession = Session::get('set_data');
        //return $getUser;
        return view('form.manageuser',['set_data' => $getUser, 'status' => $getSession, 'edit_status' => false])->with('data','Manage User');
    }

    public function editUser($key=null){
        $session = Session::get('expanel');
        if($session['role']!='admin'){
            return redirect('permission');
        }
        $status = false;
        $getUser = UserModel::all();
        if($key!=null){
            $getEditUser = UserModel::where('id', $key)->first();
            $status = true;
        }
        $res = ['status'    => $status,
                'set_data'  => $getUser,
                'edit_data' => $getEditUser];
        //return $res;
        return view('form.edituser',['response' => $res])->with('data','Manage User');
    }

}
