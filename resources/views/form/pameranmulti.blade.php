@extends('master')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Pameran Multi
      <small></small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="#">Forms</a></li>
      <li class="active">Pameran Multi</li>
    </ol>
  </section>

  <section class="content">
    <div class="row">
      <!-- left column -->
      <div class="col-md-12">
        <!-- general form elements disabled -->
        <div class="panel panel-primary">
          <div class="panel-heading"><b>Form Isian</b></div>
          <form class="form-horizontal" role="form" action="{{Config::get('app.url')}}/pameranmulti" method="post">
            <div class="panel-body">
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="col-sm-3 control-label">Formulir</label>
                    <div class="col-sm-8 input-group">
                      <div class="input-group-addon">
                        <i class="fa fa-file-text"></i>
                      </div>
                      <input type="text" name="formulir" class="form-control"  value="RKA-SKPD 2.2.1">
                    </div><!-- /.input group -->
                  </div><!-- /.form group -->
                  <div class="form-group">
                    <label class="col-sm-3 control-label">Tahun Anggaran</label>
                    <div class="col-sm-8 input-group">
                      <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                      </div>
                      <input type="text" name="thn_anggaran" class="form-control pull-right" id="reservation" value="{{date('Y')}}">
                    </div><!-- /.input group -->
                  </div><!-- /.form group -->
                  <div class="form-group">
                    <label class="col-sm-3 control-label">Lokasi Kegiatan</label>
                    <div class="col-sm-8 input-group">
                      <div class="input-group-addon">
                        <i class="fa fa-map-marker"></i>
                      </div>
                      <input type="text" name="lokasi_kegiatan" class="form-control"  value="Kabupaten Bantul">
                    </div><!-- /.input group -->
                  </div><!-- /.form group -->
                  <div class="form-group">
                    <label class="col-sm-3 control-label">Sasaran Kegiatan</label>
                    <div class="col-sm-8 input-group">
                      <div class="input-group-addon">
                        <i class="fa fa-dot-circle-o"></i>
                      </div>
                      <input type="text" name="sasaran" class="form-control">
                    </div><!-- /.input group -->
                  </div><!-- /.form group -->
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="col-sm-3 control-label">Capaian Program</label>
                    <div class="col-sm-8 input-group">
                      <div class="input-group-addon">
                        <i class="fa fa-star"></i>
                      </div>
                      <input type="text" name="capaian" class="form-control">
                    </div><!-- /.input group -->
                  </div><!-- /.form group -->
                  <div class="form-group">
                    <label class="col-sm-3 control-label">Masukkan</label>
                    <div class="col-sm-8 input-group">
                      <div class="input-group-addon">
                        <i class="fa fa-download"></i>
                      </div>
                      <input type="text" name="masukkan" class="form-control">
                    </div><!-- /.input group -->
                  </div><!-- /.form group -->
                  <div class="form-group">
                    <label class="col-sm-3 control-label">Keluaran</label>
                    <div class="col-sm-8 input-group">
                      <div class="input-group-addon">
                        <i class="fa fa-upload"></i>
                      </div>
                      <input type="text" name="keluaran" class="form-control">
                    </div><!-- /.input group -->
                  </div><!-- /.form group -->
                  <div class="form-group">
                    <label class="col-sm-3 control-label">Hasil</label>
                    <div class="col-sm-8 input-group">
                      <div class="input-group-addon">
                        <i class="fa fa-thumbs-up"></i>
                      </div>
                      <input type="text" name="hasil" class="form-control">
                    </div><!-- /.input group -->
                  </div><!-- /.form group -->
                </div>
              </div>
              <hr>  
               <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <div class="col-sm-12">
                      <div class="row">
                        <label class="col-sm-12">Honorium Tim Pengadaan Barang dan Jasa</label>
                      </div>
                      <div class="row">
                        <p class="col-sm-12">Honor Pejabat Pembuat Komitmen</p>
                      </div>
                      <div class="row">
                        <div class="col-sm-4">
                          <div class="input-group">
                            <input type="text" class="form-control" id="vol_tim" name="vol_tim" placeholder="Volume"  onkeyup="totalHarga('vol_tim','harga_tim','jumlah_tim')">
                            <span class="input-group-addon">ok</span>
                          </div>
                        </div>
                        <p class="col-sm-4" id="harga_tim">Rp 300.000,00</p>
                        <p class="col-sm-4" id="jumlah_tim">Rp 0,00</p>
                      </div>    
                    </div>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <div class="col-sm-12">
                      <div class="row">
                        <label class="col-sm-12"></label>
                      </div>
                      <br>
                      <div class="row">
                        <p class="col-sm-12">Honor Pejabat Pengadaan</p>
                      </div>
                      <div class="row">
                        <div class="col-sm-4">
                          <div class="input-group">
                            <input type="text" class="form-control" id="vol_hnr" name="vol_hnr" placeholder="Volume"  onkeyup="totalHarga('vol_hnr','harga_hnr','jumlah_hnr')">
                            <span class="input-group-addon">ok</span>
                          </div>
                        </div>
                        <p class="col-sm-4" id="harga_hnr">Rp 250.000,00</p>
                        <p class="col-sm-4" id="jumlah_hnr">Rp 0,00</p>
                      </div>
                    </div>
                  </div>
                </div>            
              </div>
              <hr> 
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <div class="col-sm-12">
                      <div class="row">
                        <label class="col-sm-12">Belanja Barang dan Jasa</label>
                      </div>
                      <div class="row">
                        <p class="col-sm-12">Belanja ATK Pameran IFFINA</p>
                      </div>
                      <div class="row">
                        <div class="col-sm-4">
                          <div class="input-group">
                            <input type="text" class="form-control" id="vol_pmr" name="vol_pmr" placeholder="Volume"  onkeyup="totalHarga('vol_pmr','harga_pmr','jumlah_pmr')">
                            <span class="input-group-addon">kgt</span>
                          </div>
                        </div>
                        <p class="col-sm-4" id="harga_pmr">Rp 300.000,00</p>
                        <p class="col-sm-4" id="jumlah_pmr">Rp 0,00</p>
                      </div>
                      <br>
                      <div class="row">
                        <p class="col-sm-12">Belanja ATK Pameran Inacraft</p>
                      </div>
                      <div class="row">
                        <div class="col-sm-4">
                          <div class="input-group">
                            <input type="text" class="form-control" id="vol_icf" name="vol_icf" placeholder="Volume"  onkeyup="totalHarga('vol_icf','harga_icf','jumlah_icf')">
                            <span class="input-group-addon">kgt</span>
                          </div>
                        </div>
                        <p class="col-sm-4" id="harga_icf">Rp 350.000,00</p>
                        <p class="col-sm-4" id="jumlah_icf">Rp 0,00</p>
                      </div>
                      <br>
                      <div class="row">
                        <p class="col-sm-12">Belanja ATK Pameran Jakarta Fair</p>
                      </div>
                      <div class="row">
                        <div class="col-sm-4">
                          <div class="input-group">
                            <input type="text" class="form-control" id="vol_jkt" name="vol_jkt" placeholder="Volume"  onkeyup="totalHarga('vol_jkt','harga_jkt','jumlah_jkt')">
                            <span class="input-group-addon">kgt</span>
                          </div>
                        </div>
                        <p class="col-sm-4" id="harga_jkt">Rp 4.000.000,00</p>
                        <p class="col-sm-4" id="jumlah_jkt">Rp 0,00</p>
                      </div>
                      <br>
                      <div class="row">
                        <p class="col-sm-12">Belanja ATK Pameran TEI</p>
                      </div>
                      <div class="row">
                        <div class="col-sm-4">
                          <div class="input-group">
                            <input type="text" class="form-control" id="vol_tei" name="vol_tei" placeholder="Volume"  onkeyup="totalHarga('vol_tei','harga_tei','jumlah_tei')">
                            <span class="input-group-addon">kgt</span>
                          </div>
                        </div>
                        <p class="col-sm-4" id="harga_tei">Rp 300.000,00</p>
                        <p class="col-sm-4" id="jumlah_tei">Rp 0,00</p>
                      </div>
                      <br>
                      <div class="row">
                        <p class="col-sm-12">Belanja Dokumetasi Pameran Fashion & Art Festival Makassar</p>
                      </div>
                      <div class="row">
                        <div class="col-sm-4">
                          <div class="input-group">
                            <input type="text" class="form-control" id="vol_afm" name="vol_afm" placeholder="Volume"  onkeyup="totalHarga('vol_afm','harga_afm','jumlah_afm')">
                            <span class="input-group-addon">kgt</span>
                          </div>
                        </div>
                        <p class="col-sm-4" id="harga_afm">Rp 300.000,00</p>
                        <p class="col-sm-4" id="jumlah_afm">Rp 0,00</p>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <div class="col-sm-12">
                      <div class="row">
                        <label class="col-sm-12">Belanja Jasa Kantor</label>
                      </div>
                      <div class="row">
                        <p class="col-sm-12">Belanja Dokumentasi Pameran IFFINA</p>
                      </div>
                      <div class="row">
                        <div class="col-sm-4">
                          <div class="input-group">
                            <input type="text" class="form-control" id="vol_dmr" name="vol_dmr" placeholder="Volume"  onkeyup="totalHarga('vol_dmr','harga_dmr','jumlah_dmr')">
                            <span class="input-group-addon">kgt</span>
                          </div>
                        </div>
                        <p class="col-sm-4" id="harga_dmr">Rp 2.500,00</p>
                        <p class="col-sm-4" id="jumlah_dmr">Rp 0,00</p>
                      </div>
                      <br>
                      <div class="row">
                        <p class="col-sm-12">Belanja Dokumentasi Pameran Inacraft</p>
                      </div>
                      <div class="row">
                        <div class="col-sm-4">
                          <div class="input-group">
                            <input type="text" class="form-control" id="vol_dcf" name="vol_dcf" placeholder="Volume"  onkeyup="totalHarga('vol_dcf','harga_dcf','jumlah_dcf')">
                            <span class="input-group-addon">kgt</span>
                          </div>
                        </div>
                        <p class="col-sm-4" id="harga_dcf">Rp 2.500,00</p>
                        <p class="col-sm-4" id="jumlah_dcf">Rp 0,00</p>
                      </div>
                      <br>
                      <div class="row">
                        <p class="col-sm-12">Belanja Dokumentasi Pameran Jakarta Fair</p>
                      </div>
                      <div class="row">
                        <div class="col-sm-4">
                          <div class="input-group">
                            <input type="text" class="form-control" id="vol_dkt" name="vol_dkt" placeholder="Volume"  onkeyup="totalHarga('vol_dkt','harga_dkt','jumlah_dkt')">
                            <span class="input-group-addon">kgt</span>
                          </div>
                        </div>
                        <p class="col-sm-4" id="harga_dkt">Rp 2.500,00</p>
                        <p class="col-sm-4" id="jumlah_dkt">Rp 0,00</p>
                      </div>
                      <br>
                      <div class="row">
                        <p class="col-sm-12">Belanja Dokumetasi Pameran TEI</p>
                      </div>
                      <div class="row">
                        <div class="col-sm-4">
                          <div class="input-group">
                            <input type="text" class="form-control" id="vol_dei" name="vol_dei" placeholder="Volume"  onkeyup="totalHarga('vol_dei','harga_dei','jumlah_dei')">
                            <span class="input-group-addon">kgt</span>
                          </div>
                        </div>
                        <p class="col-sm-4" id="harga_dei">Rp 2.500,00</p>
                        <p class="col-sm-4" id="jumlah_dei">Rp 0,00</p>
                      </div>
                      <br>
                      <div class="row">
                        <p class="col-sm-12">Belanja Dokumetasi Pameran Fashion & Art Festival Makassar</p>
                      </div>
                      <div class="row">
                        <div class="col-sm-4">
                          <div class="input-group">
                            <input type="text" class="form-control" id="vol_dfm" name="vol_dfm" placeholder="Volume"  onkeyup="totalHarga('vol_dfm','harga_dfm','jumlah_dfm')">
                            <span class="input-group-addon">kgt</span>
                          </div>
                        </div>
                        <p class="col-sm-4" id="harga_dfm">Rp 2.500,00</p>
                        <p class="col-sm-4" id="jumlah_dfm">Rp 0,00</p>
                      </div>
                    </div>  
                  </div>
                </div>      
              </div>
              <hr> 
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <div class="col-sm-12">
                      <div class="row">
                        <label class="col-sm-12">Belanja Cetak Pengadaan</label>
                      </div>
                      <div class="row">
                        <p class="col-sm-12">Belanja Cetak Leaflet Pameran IFFINA</p>
                      </div>
                      <div class="row">
                        <div class="col-sm-4">
                          <div class="input-group">
                            <input type="text" class="form-control" id="vol_clm" name="vol_clm" placeholder="Volume"  onkeyup="totalHarga('vol_clm','harga_clm','jumlah_clm')">
                            <span class="input-group-addon">Exp</span>
                          </div>
                        </div>
                        <p class="col-sm-4" id="harga_clm">Rp 2.500,00</p>
                        <p class="col-sm-4" id="jumlah_clm">Rp 0,00</p>
                      </div>
                      <br>
                      <div class="row">
                        <p class="col-sm-12">Belanja Cetak Leaflet Pameran Inacraft</p>
                      </div>
                      <div class="row">
                        <div class="col-sm-4">
                          <div class="input-group">
                            <input type="text" class="form-control" id="vol_cln" name="vol_cln" placeholder="Volume"  onkeyup="totalHarga('vol_cln','harga_cln','jumlah_cln')">
                            <span class="input-group-addon">Exp</span>
                          </div>
                        </div>
                        <p class="col-sm-4" id="harga_cln">Rp 2.500,00</p>
                        <p class="col-sm-4" id="jumlah_cln">Rp 0,00</p>
                      </div>
                      <br>
                      <div class="row">
                        <p class="col-sm-12">Belanja Cetak Leaflet Jakarta Fair</p>
                      </div>
                      <div class="row">
                        <div class="col-sm-4">
                          <div class="input-group">
                            <input type="text" class="form-control" id="vol_clp" name="vol_clp" placeholder="Volume"  onkeyup="totalHarga('vol_clp','harga_clp','jumlah_clp')">
                            <span class="input-group-addon">Exp</span>
                          </div>
                        </div>
                        <p class="col-sm-4" id="harga_clp">Rp 2.500,00</p>
                        <p class="col-sm-4" id="jumlah_clp">Rp 0,00</p>
                      </div>
                      <br>
                      <div class="row">
                        <p class="col-sm-12">Belanja Cetak Leaflet Pameran TEI</p>
                      </div>
                      <div class="row">
                        <div class="col-sm-4">
                          <div class="input-group">
                            <input type="text" class="form-control" id="vol_clq" name="vol_clq" placeholder="Volume"  onkeyup="totalHarga('vol_clq','harga_clq','jumlah_clq')">
                            <span class="input-group-addon">Exp</span>
                          </div>
                        </div>
                        <p class="col-sm-4" id="harga_clq">Rp 2.500,00</p>
                        <p class="col-sm-4" id="jumlah_clq">Rp 0,00</p>
                      </div>
                      <br>
                      <div class="row">
                        <p class="col-sm-12">Belanja Cetak Leaflet Pameran Fashion & Art Festival Makassar</p>
                      </div>
                      <div class="row">
                        <div class="col-sm-4">
                          <div class="input-group">
                            <input type="text" class="form-control" id="vol_clr" name="vol_clr" placeholder="Volume"  onkeyup="totalHarga('vol_clr','harga_clr','jumlah_clr')">
                            <span class="input-group-addon">kgt</span>
                          </div>
                        </div>
                        <p class="col-sm-4" id="harga_clr">Rp 2.500,00</p>
                        <p class="col-sm-4" id="jumlah_clr">Rp 0,00</p>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <div class="col-sm-12">
                      <div class="row">
                        <label class="col-sm-12">Belanja Pengadaan</label>
                      </div>
                      <div class="row">
                        <p class="col-sm-12">Belanja Pengadaan Pameran IFFINA</p>
                      </div>
                      <div class="row">
                        <div class="col-sm-4">
                          <div class="input-group">
                            <input type="text" class="form-control" id="vol_ska" name="vol_ska" placeholder="Volume"  onkeyup="totalHarga('vol_ska','harga_ska','jumlah_ska')">
                            <span class="input-group-addon">lbr</span>
                          </div>
                        </div>
                        <p class="col-sm-4" id="harga_ska">Rp 125,00</p>
                        <p class="col-sm-4" id="jumlah_ska">Rp 0,00</p>
                      </div>
                      <br>
                      <div class="row">
                        <p class="col-sm-12">Belanja Pengadaan  Pameran Inacraft</p>
                      </div>
                      <div class="row">
                        <div class="col-sm-4">
                          <div class="input-group">
                            <input type="text" class="form-control" id="vol_skb" name="vol_skb" placeholder="Volume"  onkeyup="totalHarga('vol_skb','harga_skb','jumlah_skb')">
                            <span class="input-group-addon">lbr</span>
                          </div>
                        </div>
                        <p class="col-sm-4" id="harga_skb">Rp 125,00</p>
                        <p class="col-sm-4" id="jumlah_skb">Rp 0,00</p>
                      </div>
                      <br>
                      <div class="row">
                        <p class="col-sm-12">Belanja Pengadaan  Pameran Jakarta Fair</p>
                      </div>
                      <div class="row">
                        <div class="col-sm-4">
                          <div class="input-group">
                            <input type="text" class="form-control" id="vol_skc" name="vol_skc" placeholder="Volume"  onkeyup="totalHarga('vol_skc','harga_skc','jumlah_skc')">
                            <span class="input-group-addon">lbr</span>
                          </div>
                        </div>
                        <p class="col-sm-4" id="harga_skc">Rp 125,00</p>
                        <p class="col-sm-4" id="jumlah_skc">Rp 0,00</p>
                      </div>
                      <br>
                      <div class="row">
                        <p class="col-sm-12">Belanja Pengadaan Pameran TEI</p>
                      </div>
                      <div class="row">
                        <div class="col-sm-4">
                          <div class="input-group">
                            <input type="text" class="form-control" id="vol_skd" name="vol_skd" placeholder="Volume"  onkeyup="totalHarga('vol_skd','harga_skd','jumlah_skd')">
                            <span class="input-group-addon">lbr</span>
                          </div>
                        </div>
                        <p class="col-sm-4" id="harga_skd">Rp 125,00</p>
                        <p class="col-sm-4" id="jumlah_skd">Rp 0,00</p>
                      </div>
                      <br>
                      <div class="row">
                        <p class="col-sm-12">Belanja Pengadaan  Pameran Fashion & Art Festival Makassar</p>
                      </div>
                      <div class="row">
                        <div class="col-sm-4">
                          <div class="input-group">
                            <input type="text" class="form-control" id="vol_skf" name="vol_skf" placeholder="Volume"  onkeyup="totalHarga('vol_skf','harga_skf','jumlah_skf')">
                            <span class="input-group-addon">kgt</span>
                          </div>
                        </div>
                        <p class="col-sm-4" id="harga_skf">Rp 125,00</p>
                        <p class="col-sm-4" id="jumlah_skf">Rp 0,00</p>
                      </div>
                    </div>
                  </div>
                </div>
               </div> 
              <hr>
              <div class="row">  
                <div class="col-md-6">
                  <div class="form-group">
                    <div class="col-sm-12">
                      <div class="row">
                        <label class="col-sm-12">Belanja Sewa</label>
                      </div>
                      <div class="row">
                        <p class="col-sm-12">Belanja Sewa Kapling/Stand IFFINA</p>
                      </div>
                      <div class="row">
                        <div class="col-sm-4">
                          <div class="input-group">
                            <input type="text" class="form-control" id="vol_eka" name="vol_eka" placeholder="Volume"  onkeyup="totalHarga('vol_eka','harga_eka','jumlah_eka')">
                            <span class="input-group-addon">m</span>
                          </div>
                        </div>
                        <p class="col-sm-4" id="harga_eka">Rp 400.000,00</p>
                        <p class="col-sm-4" id="jumlah_eka">Rp 0,00</p>
                      </div>
                      <br>
                      <div class="row">
                        <p class="col-sm-12">Belanja Sewa Kapling/Stand Pameran Inacraft</p>
                      </div>
                      <div class="row">
                        <div class="col-sm-4">
                          <div class="input-group">
                            <input type="text" class="form-control" id="vol_ekb" name="vol_ekb" placeholder="Volume"  onkeyup="totalHarga('vol_ekb','harga_ekb','jumlah_ekb')">
                            <span class="input-group-addon">m</span>
                          </div>
                        </div>
                        <p class="col-sm-4" id="harga_ekb">Rp 400.000,00</p>
                        <p class="col-sm-4" id="jumlah_ekb">Rp 0,00</p>
                      </div>
                      <br>
                      <div class="row">
                        <p class="col-sm-12">Belanja Sewa Kapling/Stand Pameran Jakarta Fair</p>
                      </div>
                      <div class="row">
                        <div class="col-sm-4">
                          <div class="input-group">
                            <input type="text" class="form-control" id="vol_ekc" name="vol_ekc" placeholder="Volume"  onkeyup="totalHarga('vol_ekc','harga_ekc','jumlah_ekc')">
                            <span class="input-group-addon">m</span>
                          </div>
                        </div>
                        <p class="col-sm-4" id="harga_ekc">Rp 400.000,00</p>
                        <p class="col-sm-4" id="jumlah_ekc">Rp 0,00</p>
                      </div>
                      <br>
                      <div class="row">
                        <p class="col-sm-12">Belanja Sewa Kapling/Stand Pameran TEI</p>
                      </div>
                      <div class="row">
                        <div class="col-sm-4">
                          <div class="input-group">
                            <input type="text" class="form-control" id="vol_ekd" name="vol_ekd" placeholder="Volume"  onkeyup="totalHarga('vol_ekd','harga_ekd','jumlah_ekd')">
                            <span class="input-group-addon">m</span>
                          </div>
                        </div>
                        <p class="col-sm-4" id="harga_ekd">Rp 400.000,00</p>
                        <p class="col-sm-4" id="jumlah_ekd">Rp 0,00</p>
                      </div>
                      <br>
                      <div class="row">
                        <p class="col-sm-12">Belanja Sewa Kapling/Stand Pameran Fashion & Art Festival Makassar</p>
                      </div>
                      <div class="row">
                        <div class="col-sm-4">
                          <div class="input-group">
                            <input type="text" class="form-control" id="vol_ekf" name="vol_ekf" placeholder="Volume"  onkeyup="totalHarga('vol_ekf','harga_ekf','jumlah_ekf')">
                            <span class="input-group-addon">m</span>
                          </div>
                        </div>
                        <p class="col-sm-4" id="harga_ekf">Rp 400.000,00</p>
                        <p class="col-sm-4" id="jumlah_ekf">Rp 0,00</p>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <div class="col-sm-12">
                      <div class="row">
                        <label class="col-sm-12">Belanja Sewa Sarana Mobilitas</label>
                      </div>
                      <div class="row">
                        <p class="col-sm-12">Belanja Sewa Mobil Pameran IFFINA</p>
                      </div>
                      <div class="row">
                        <div class="col-sm-4">
                          <div class="input-group">
                            <input type="text" class="form-control" id="vol_mka" name="vol_mka" placeholder="Volume"  onkeyup="totalHarga('vol_mka','harga_mka','jumlah_mka')">
                            <span class="input-group-addon">kgt</span>
                          </div>
                        </div>
                        <p class="col-sm-4" id="harga_mka">Rp 9.000.000,00</p>
                        <p class="col-sm-4" id="jumlah_mka">Rp 0,00</p>
                      </div>
                      <br>
                      <div class="row">
                        <p class="col-sm-12">Belanja Sewa Mobil Pameran Inacraft</p>
                      </div>
                      <div class="row">
                        <div class="col-sm-4">
                          <div class="input-group">
                            <input type="text" class="form-control" id="vol_mkb" name="vol_mkb" placeholder="Volume"  onkeyup="totalHarga('vol_mkb','harga_mkb','jumlah_mkb')">
                            <span class="input-group-addon">kgt</span>
                          </div>
                        </div>
                        <p class="col-sm-4" id="harga_mkb">Rp 9.000.000,00</p>
                        <p class="col-sm-4" id="jumlah_mkb">Rp 0,00</p>
                      </div>
                      <br>
                      <div class="row">
                        <p class="col-sm-12">Belanja Sewa Mobil Pameran Jakarta Fair</p>
                      </div>
                      <div class="row">
                        <div class="col-sm-4">
                          <div class="input-group">
                            <input type="text" class="form-control" id="vol_mkc" name="vol_mkc" placeholder="Volume"  onkeyup="totalHarga('vol_mkc','harga_mkc','jumlah_mkc')">
                            <span class="input-group-addon">kgt</span>
                          </div>
                        </div>
                        <p class="col-sm-4" id="harga_mkc">Rp 30.000.000,00</p>
                        <p class="col-sm-4" id="jumlah_mkc">Rp 0,00</p>
                      </div>
                      <br>
                      <div class="row">
                        <p class="col-sm-12">Belanja Sewa Mobil Pameran TEI</p>
                      </div>
                      <div class="row">
                        <div class="col-sm-4">
                          <div class="input-group">
                            <input type="text" class="form-control" id="vol_mkd" name="vol_mkd" placeholder="Volume"  onkeyup="totalHarga('vol_mkd','harga_mkd','jumlah_mkd')">
                            <span class="input-group-addon">kgt</span>
                          </div>
                        </div>
                        <p class="col-sm-4" id="harga_mkd">Rp 9.000.000,00</p>
                        <p class="col-sm-4" id="jumlah_mkd">Rp 0,00</p>
                      </div>
                      <br>
                      <div class="row">
                        <p class="col-sm-12">Belanja Sewa Mobil Pameran Fashion & Art Festival Makassar</p>
                      </div>
                      <div class="row">
                        <div class="col-sm-4">
                          <div class="input-group">
                            <input type="text" class="form-control" id="vol_mkf" name="vol_mkf" placeholder="Volume"  onkeyup="totalHarga('vol_mkf','harga_mkf','jumlah_mkf')">
                            <span class="input-group-addon">kgt</span>
                          </div>
                        </div>
                        <p class="col-sm-4" id="harga_mkf">Rp 10.000.000,00</p>
                        <p class="col-sm-4" id="jumlah_mkf">Rp 0,00</p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <hr>
               <div class="row"> 
                <div class="col-md-6">
                  <div class="form-group">
                    <div class="col-sm-12">
                      <div class="row">
                        <label class="col-sm-12">Belanja Sewa Peralatan Disply</label>
                      </div>
                      <div class="row">
                        <p class="col-sm-12">Belanja Disply Pameran IFFINA</p>
                      </div>
                      <div class="row">
                        <div class="col-sm-4">
                          <div class="input-group">
                            <input type="text" class="form-control" id="vol_rka" name="vol_rka" placeholder="Volume"  onkeyup="totalHarga('vol_rka','harga_rka','jumlah_rka')">
                            <span class="input-group-addon">kgt</span>
                          </div>
                        </div>
                        <p class="col-sm-4" id="harga_rka">Rp 25.000.000,00</p>
                        <p class="col-sm-4" id="jumlah_rka">Rp 0,00</p>
                      </div>
                      <br>
                      <div class="row">
                        <p class="col-sm-12">Belanja Disply Pameran Inacraft</p>
                      </div>
                      <div class="row">
                        <div class="col-sm-4">
                          <div class="input-group">
                            <input type="text" class="form-control" id="vol_rkb" name="vol_rkb" placeholder="Volume"  onkeyup="totalHarga('vol_rkb','harga_rkb','jumlah_rkb')">
                            <span class="input-group-addon">kgt</span>
                          </div>
                        </div>
                        <p class="col-sm-4" id="harga_rkb">Rp 30.000.000,00</p>
                        <p class="col-sm-4" id="jumlah_rkb">Rp 0,00</p>
                      </div>
                      <br>
                      <div class="row">
                        <p class="col-sm-12">Belanja Disply Pameran Jakarta Fair</p>
                      </div>
                      <div class="row">
                        <div class="col-sm-4">
                          <div class="input-group">
                            <input type="text" class="form-control" id="vol_rkc" name="vol_rkc" placeholder="Volume"  onkeyup="totalHarga('vol_rkc','harga_rkc','jumlah_rkc')">
                            <span class="input-group-addon">kgt</span>
                          </div>
                        </div>
                        <p class="col-sm-4" id="harga_rkc">Rp 30.000.000,00</p>
                        <p class="col-sm-4" id="jumlah_rkc">Rp 0,00</p>
                      </div>
                      <br>
                      <div class="row">
                        <p class="col-sm-12">Belanja Disply Pameran TEI</p>
                      </div>
                      <div class="row">
                        <div class="col-sm-4">
                          <div class="input-group">
                            <input type="text" class="form-control" id="vol_rkd" name="vol_rkd" placeholder="Volume"  onkeyup="totalHarga('vol_rkd','harga_rkd','jumlah_rkd')">
                            <span class="input-group-addon">kgt</span>
                          </div>
                        </div>
                        <p class="col-sm-4" id="harga_rkd">Rp 25.000.000,00</p>
                        <p class="col-sm-4" id="jumlah_rkd">Rp 0,00</p>
                      </div>
                      <br>
                      <div class="row">
                        <p class="col-sm-12">Belanja Disply Pameran Fashion & Art Festival Makassar</p>
                      </div>
                      <div class="row">
                        <div class="col-sm-4">
                          <div class="input-group">
                            <input type="text" class="form-control" id="vol_rkf" name="vol_rkf" placeholder="Volume"  onkeyup="totalHarga('vol_rkf','harga_rkf','jumlah_rkf')">
                            <span class="input-group-addon">kgt</span>
                          </div>
                        </div>
                        <p class="col-sm-4" id="harga_rkf">Rp 25.000.000,00</p>
                        <p class="col-sm-4" id="jumlah_rkf">Rp 0,00</p>
                      </div>
                    </div>
                  </div>
                </div>      
                <div class="col-md-6">
                  <div class="form-group">
                    <div class="col-sm-12">
                      <div class="row">
                        <label class="col-sm-12">Belanja Makanana dan Minuman </label>
                      </div>
                      <div class="row">
                        <p class="col-sm-12">Belanja Makan Minum Pameran IFFINA</p>
                      </div>
                      <div class="row">
                        <div class="col-sm-4">
                          <div class="input-group">
                            <input type="text" class="form-control" id="vol_kka" name="vol_kka" placeholder="Volume"  onkeyup="totalHarga('vol_kka','harga_kka','jumlah_kka')">
                            <span class="input-group-addon">OH</span>
                          </div>
                        </div>
                        <p class="col-sm-4" id="harga_kka">Rp 7.500,00</p>
                        <p class="col-sm-4" id="jumlah_kka">Rp 0,00</p>
                      </div>
                      <br>
                      <div class="row">
                        <p class="col-sm-12">Belanja Makan Minum Pameran Inacraft</p>
                      </div>
                      <div class="row">
                        <div class="col-sm-4">
                          <div class="input-group">
                            <input type="text" class="form-control" id="vol_kkb" name="vol_kkb" placeholder="Volume"  onkeyup="totalHarga('vol_kkb','harga_kkb','jumlah_kkb')">
                            <span class="input-group-addon">OH</span>
                          </div>
                        </div>
                        <p class="col-sm-4" id="harga_kkb">Rp 7.500,00</p>
                        <p class="col-sm-4" id="jumlah_kkb">Rp 0,00</p>
                      </div>
                      <br>
                      <div class="row">
                        <p class="col-sm-12">Belanja Makan Minum Pameran Jakarta Fair</p>
                      </div>
                      <div class="row">
                        <div class="col-sm-4">
                          <div class="input-group">
                            <input type="text" class="form-control" id="vol_kkc" name="vol_kkc" placeholder="Volume"  onkeyup="totalHarga('vol_kkc','harga_kkc','jumlah_kkc')">
                            <span class="input-group-addon">OH</span>
                          </div>
                        </div>
                        <p class="col-sm-4" id="harga_kkc">Rp 7.500,00</p>
                        <p class="col-sm-4" id="jumlah_kkc">Rp 0,00</p>
                      </div>
                      <br>
                      <div class="row">
                        <p class="col-sm-12">Belanja Makan Minum Pameran TEI</p>
                      </div>
                      <div class="row">
                        <div class="col-sm-4">
                          <div class="input-group">
                            <input type="text" class="form-control" id="vol_kkd" name="vol_kkd" placeholder="Volume"  onkeyup="totalHarga('vol_kkd','harga_kkd','jumlah_kkd')">
                            <span class="input-group-addon">OH</span>
                          </div>
                        </div>
                        <p class="col-sm-4" id="harga_kkd">Rp 7.500,00</p>
                        <p class="col-sm-4" id="jumlah_kkd">Rp 0,00</p>
                      </div>
                      <br>
                      <div class="row">
                        <p class="col-sm-12">Belanja Makan Minum Pameran Fashion & Art Festival Makassar</p>
                      </div>
                      <div class="row">
                        <div class="col-sm-4">
                          <div class="input-group">
                            <input type="text" class="form-control" id="vol_kkf" name="vol_kkf" placeholder="Volume"  onkeyup="totalHarga('vol_kkf','harga_kkf','jumlah_kkf')">
                            <span class="input-group-addon">OH</span>
                          </div>
                        </div>
                        <p class="col-sm-4" id="harga_kkf">Rp 7.500,00</p>
                        <p class="col-sm-4" id="jumlah_kkf">Rp 0,00</p>
                      </div>
                    </div>
                  </div>
                </div>
               </div>
               <hr>
               <div class="row"> 
                <div class="col-md-6">
                  <div class="form-group">
                    <div class="col-sm-12">
                      <div class="row">
                        <label class="col-sm-12">Belanja Perjalanan Dinas Daerah</label>
                      </div>
                      <div class="row">
                        <p class="col-sm-12">Goll III Pameran IFFINA</p>
                      </div>
                      <div class="row">
                        <div class="col-sm-4">
                          <div class="input-group">
                            <input type="text" class="form-control" id="vol_lka" name="vol_lka" placeholder="Volume"  onkeyup="totalHarga('vol_lka','harga_lka','jumlah_lka')">
                            <span class="input-group-addon">OH</span>
                          </div>
                        </div>
                        <p class="col-sm-4" id="harga_lka">Rp 40.000,00</p>
                        <p class="col-sm-4" id="jumlah_lka">Rp 0,00</p>
                      </div>
                      <br>
                      <div class="row">
                        <p class="col-sm-12">Gol III Pameran Inacraft</p>
                      </div>
                      <div class="row">
                        <div class="col-sm-4">
                          <div class="input-group">
                            <input type="text" class="form-control" id="vol_lkb" name="vol_lkb" placeholder="Volume"  onkeyup="totalHarga('vol_lkb','harga_lkb','jumlah_lkb')">
                            <span class="input-group-addon">OH</span>
                          </div>
                        </div>
                        <p class="col-sm-4" id="harga_lkb">Rp 40.000,00</p>
                        <p class="col-sm-4" id="jumlah_lkb">Rp 0,00</p>
                      </div>
                      <br>
                      <div class="row">
                        <p class="col-sm-12">Gol III Pameran Jakarta Fair</p>
                      </div>
                      <div class="row">
                        <div class="col-sm-4">
                          <div class="input-group">
                            <input type="text" class="form-control" id="vol_lkc" name="vol_lkc" placeholder="Volume"  onkeyup="totalHarga('vol_lkc','harga_lkc','jumlah_lkc')">
                            <span class="input-group-addon">OH</span>
                          </div>
                        </div>
                        <p class="col-sm-4" id="harga_lkc">Rp 40.000,00</p>
                        <p class="col-sm-4" id="jumlah_lkc">Rp 0,00</p>
                      </div>
                      <br>
                      <div class="row">
                        <p class="col-sm-12">Gol III Pameran TEI</p>
                      </div>
                      <div class="row">
                        <div class="col-sm-4">
                          <div class="input-group">
                            <input type="text" class="form-control" id="vol_lkd" name="vol_lkd" placeholder="Volume"  onkeyup="totalHarga('vol_lkd','harga_lkd','jumlah_lkd')">
                            <span class="input-group-addon">OH</span>
                          </div>
                        </div>
                        <p class="col-sm-4" id="harga_lkd">Rp 40.000,00</p>
                        <p class="col-sm-4" id="jumlah_lkd">Rp 0,00</p>
                      </div>
                      <br>
                      <div class="row">
                        <p class="col-sm-12">Gol III Pameran Fashion & Art Festival Makassar</p>
                      </div>
                      <div class="row">
                        <div class="col-sm-4">
                          <div class="input-group">
                            <input type="text" class="form-control" id="vol_lkf" name="vol_lkf" placeholder="Volume"  onkeyup="totalHarga('vol_lkf','harga_lkf','jumlah_lkf')">
                            <span class="input-group-addon">OH</span>
                          </div>
                        </div>
                        <p class="col-sm-4" id="harga_lkf">Rp 40.000,00</p>
                        <p class="col-sm-4" id="jumlah_lkf">Rp 0,00</p>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <div class="col-sm-12">
                      <div class="row">
                        <label class="col-sm-12">Pameran IFFINA </label>
                      </div>
                      <div class="row">
                        <p class="col-sm-12">Gol IV Lumpsum : 1 org x 2 hr</p>
                      </div>
                      <div class="row">
                        <div class="col-sm-4">
                          <div class="input-group">
                            <input type="text" class="form-control" id="vol_oka" name="vol_oka" placeholder="Volume"  onkeyup="totalHarga('vol_oka','harga_oka','jumlah_oka')">
                            <span class="input-group-addon">OH</span>
                          </div>
                        </div>
                        <p class="col-sm-4" id="harga_oka">Rp 1.700.000,00</p>
                        <p class="col-sm-4" id="jumlah_oka">Rp 0,00</p>
                      </div>
                      <br>
                      <div class="row">
                        <p class="col-sm-12">Trasport PP 1org</p>
                      </div>
                      <div class="row">
                        <div class="col-sm-4">
                          <div class="input-group">
                            <input type="text" class="form-control" id="vol_vka" name="vol_vka" placeholder="Volume"  onkeyup="totalHarga('vol_vka','harga_vka','jumlah_vka')">
                            <span class="input-group-addon">OK</span>
                          </div>
                        </div>
                        <p class="col-sm-4" id="harga_vka">Rp 2.700.000,00</p>
                        <p class="col-sm-4" id="jumlah_vka">Rp 0,00</p>
                      </div>
                      <br>
                      <div class="row">
                        <p class="col-sm-12">Gol IV Lumpsum : 1 org x 3 hr</p>
                      </div>
                      <div class="row">
                        <div class="col-sm-4">
                          <div class="input-group">
                            <input type="text" class="form-control" id="vol_okb" name="vol_okb" placeholder="Volume"  onkeyup="totalHarga('vol_okb','harga_okb','jumlah_okb')">
                            <span class="input-group-addon">OH</span>
                          </div>
                        </div>
                        <p class="col-sm-4" id="harga_okb">Rp 1.700.000,00</p>
                        <p class="col-sm-4" id="jumlah_okb">Rp 0,00</p>
                      </div>
                      <br>
                      <div class="row">
                        <p class="col-sm-12">Trasport PP 1org</p>
                      </div>
                      <div class="row">
                        <div class="col-sm-4">
                          <div class="input-group">
                            <input type="text" class="form-control" id="vol_vkb" name="vol_vkb" placeholder="Volume"  onkeyup="totalHarga('vol_vkb','harga_vkb','jumlah_vkb')">
                            <span class="input-group-addon">OK</span>
                          </div>
                        </div>
                        <p class="col-sm-4" id="harga_vkb">Rp 900.000,00</p>
                        <p class="col-sm-4" id="jumlah_vkb">Rp 0,00</p>
                      </div>
                      <br>
                      <div class="row">
                        <p class="col-sm-12">Gol III Lumpsum : 5 org x 3 hr</p>
                      </div>
                      <div class="row">
                        <div class="col-sm-4">
                          <div class="input-group">
                            <input type="text" class="form-control" id="vol_okc" name="vol_okc" placeholder="Volume"  onkeyup="totalHarga('vol_okc','harga_okc','jumlah_okc')">
                            <span class="input-group-addon">OH</span>
                          </div>
                        </div>
                        <p class="col-sm-4" id="harga_okc">Rp 1.500.000,00</p>
                        <p class="col-sm-4" id="jumlah_okc">Rp 0,00</p>
                      </div>
                      <br>
                      <div class="row">
                        <p class="col-sm-12">Trasport PP 5org</p>
                      </div>
                      <div class="row">
                        <div class="col-sm-4">
                          <div class="input-group">
                            <input type="text" class="form-control" id="vol_vkc" name="vol_vkc" placeholder="Volume"  onkeyup="totalHarga('vol_vkc','harga_vkc','jumlah_vkc')">
                            <span class="input-group-addon">OK</span>
                          </div>
                        </div>
                        <p class="col-sm-4" id="harga_vkc">Rp 900.000,00</p>
                        <p class="col-sm-4" id="jumlah_vkc">Rp 0,00</p>
                      </div>
                      <br>
                      <div class="row">
                        <p class="col-sm-12">Gol III Lumpsum ( peserta Pameran )</p>
                      </div>
                      <div class="row">
                        <div class="col-sm-4">
                          <div class="input-group">
                            <input type="text" class="form-control" id="vol_zkc" name="vol_zkc" placeholder="Volume"  onkeyup="totalHarga('vol_zkc','harga_zkc','jumlah_zkc')">
                            <span class="input-group-addon">OK</span>
                          </div>
                        </div>
                        <p class="col-sm-4" id="harga_zkc">Rp 75.000,00</p>
                        <p class="col-sm-4" id="jumlah_zkc">Rp 0,00</p>
                      </div>
                    </div>
                  </div>
                </div> 
              </div>
              <hr>
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <div class="col-sm-12">
                      <div class="row">
                        <label class="col-sm-12">Pameran Inacraft </label>
                      </div>
                      <div class="row">
                        <p class="col-sm-12">Gol IV Lumpsum : 1 org x 2 hr</p>
                      </div>
                      <div class="row">
                        <div class="col-sm-4">
                          <div class="input-group">
                            <input type="text" class="form-control" id="vol_wka" name="vol_wka" placeholder="Volume"  onkeyup="totalHarga('vol_wka','harga_wka','jumlah_wka')">
                            <span class="input-group-addon">OH</span>
                          </div>
                        </div>
                        <p class="col-sm-4" id="harga_wka">Rp 1.700.000,00</p>
                        <p class="col-sm-4" id="jumlah_wka">Rp 0,00</p>
                      </div>
                      <br>
                      <div class="row">
                        <p class="col-sm-12">Trasport PP 1org</p>
                      </div>
                      <div class="row">
                        <div class="col-sm-4">
                          <div class="input-group">
                            <input type="text" class="form-control" id="vol_ika" name="vol_ika" placeholder="Volume"  onkeyup="totalHarga('vol_ika','harga_ika','jumlah_ika')">
                            <span class="input-group-addon">OK</span>
                          </div>
                        </div>
                        <p class="col-sm-4" id="harga_ika">Rp 2.700.000,00</p>
                        <p class="col-sm-4" id="jumlah_ika">Rp 0,00</p>
                      </div>
                      <br>
                      <div class="row">
                        <p class="col-sm-12">Gol IV Lumpsum : 1 org x 3 hr</p>
                      </div>
                      <div class="row">
                        <div class="col-sm-4">
                          <div class="input-group">
                            <input type="text" class="form-control" id="vol_wkb" name="vol_wkb" placeholder="Volume"  onkeyup="totalHarga('vol_wkb','harga_wkb','jumlah_wkb')">
                            <span class="input-group-addon">OH</span>
                          </div>
                        </div>
                        <p class="col-sm-4" id="harga_wkb">Rp 1.700.000,00</p>
                        <p class="col-sm-4" id="jumlah_wkb">Rp 0,00</p>
                      </div>
                      <br>
                      <div class="row">
                        <p class="col-sm-12">Trasport PP 1org</p>
                      </div>
                      <div class="row">
                        <div class="col-sm-4">
                          <div class="input-group">
                            <input type="text" class="form-control" id="vol_ikb" name="vol_ikb" placeholder="Volume"  onkeyup="totalHarga('vol_ikb','harga_ikb','jumlah_ikb')">
                            <span class="input-group-addon">OK</span>
                          </div>
                        </div>
                        <p class="col-sm-4" id="harga_ikb">Rp 900.000,00</p>
                        <p class="col-sm-4" id="jumlah_ikb">Rp 0,00</p>
                      </div>
                      <br>
                      <div class="row">
                        <p class="col-sm-12">Gol III Lumpsum : 5 org x 3 hr</p>
                      </div>
                      <div class="row">
                        <div class="col-sm-4">
                          <div class="input-group">
                            <input type="text" class="form-control" id="vol_wkc" name="vol_wkc" placeholder="Volume"  onkeyup="totalHarga('vol_wkc','harga_wkc','jumlah_wkc')">
                            <span class="input-group-addon">OH</span>
                          </div>
                        </div>
                        <p class="col-sm-4" id="harga_wkc">Rp 1.500.000,00</p>
                        <p class="col-sm-4" id="jumlah_wkc">Rp 0,00</p>
                      </div>
                      <br>
                      <div class="row">
                        <p class="col-sm-12">Trasport PP 5org</p>
                      </div>
                      <div class="row">
                        <div class="col-sm-4">
                          <div class="input-group">
                            <input type="text" class="form-control" id="vol_ikc" name="vol_ikc" placeholder="Volume"  onkeyup="totalHarga('vol_ikc','harga_ikc','jumlah_ikc')">
                            <span class="input-group-addon">OK</span>
                          </div>
                        </div>
                        <p class="col-sm-4" id="harga_ikc">Rp 900.000,00</p>
                        <p class="col-sm-4" id="jumlah_ikc">Rp 0,00</p>
                      </div>
                      <br>
                      <div class="row">
                        <p class="col-sm-12">Gol III Lumpsum ( peserta Pameran )</p>
                      </div>
                      <div class="row">
                        <div class="col-sm-4">
                          <div class="input-group">
                            <input type="text" class="form-control" id="vol_jkc" name="vol_jkc" placeholder="Volume"  onkeyup="totalHarga('vol_jkc','harga_jkc','jumlah_jkc')">
                            <span class="input-group-addon">OK</span>
                          </div>
                        </div>
                        <p class="col-sm-4" id="harga_jkc">Rp 75.000,00</p>
                        <p class="col-sm-4" id="jumlah_jkc">Rp 0,00</p>
                      </div> 
                    </div>
                  </div>
                </div> 
                <div class="col-md-6">
                  <div class="form-group">
                    <div class="col-sm-12">
                      <div class="row">
                        <label class="col-sm-12">Pameran Jakarta Fair </label>
                      </div>
                      <div class="row">
                        <p class="col-sm-12">Gol III Lumpsum</p>
                      </div>
                      <div class="row">
                        <div class="col-sm-4">
                          <div class="input-group">
                            <input type="text" class="form-control" id="vol_0ka" name="vol_0ka" placeholder="Volume"  onkeyup="totalHarga('vol_0ka','harga_0ka','jumlah_0ka')">
                            <span class="input-group-addon">OH</span>
                          </div>
                        </div>
                        <p class="col-sm-4" id="harga_0ka">Rp 1.500.000,00</p>
                        <p class="col-sm-4" id="jumlah_0ka">Rp 0,00</p>
                      </div>
                      <br>
                      <div class="row">
                        <p class="col-sm-12">Trasport </p>
                      </div>
                      <div class="row">
                        <div class="col-sm-4">
                          <div class="input-group">
                            <input type="text" class="form-control" id="vol_xkx" name="vol_xkx" placeholder="Volume"  onkeyup="totalHarga('vol_xkx','harga_xkx','jumlah_xkx')">
                            <span class="input-group-addon">OK</span>
                          </div>
                        </div>
                        <p class="col-sm-4" id="harga_xkx">Rp 900.000,00</p>
                        <p class="col-sm-4" id="jumlah_xkx">Rp 0,00</p>
                      </div>
                      <br>
                      <div class="row">
                        <p class="col-sm-12">Gol IV Lumpsum : 1 org x 2 hr</p>
                      </div>
                      <div class="row">
                        <div class="col-sm-4">
                          <div class="input-group">
                            <input type="text" class="form-control" id="vol_uka" name="vol_uka" placeholder="Volume"  onkeyup="totalHarga('vol_uka','harga_uka','jumlah_uka')">
                            <span class="input-group-addon">OH</span>
                          </div>
                        </div>
                        <p class="col-sm-4" id="harga_uka">Rp 1.700.000,00</p>
                        <p class="col-sm-4" id="jumlah_uka">Rp 0,00</p>
                      </div>
                      <br>
                      <div class="row">
                        <p class="col-sm-12">Trasport PP 1org</p>
                      </div>
                      <div class="row">
                        <div class="col-sm-4">
                          <div class="input-group">
                            <input type="text" class="form-control" id="vol_xka" name="vol_xka" placeholder="Volume"  onkeyup="totalHarga('vol_xka','harga_xka','jumlah_xka')">
                            <span class="input-group-addon">OK</span>
                          </div>
                        </div>
                        <p class="col-sm-4" id="harga_xka">Rp 2.700.000,00</p>
                        <p class="col-sm-4" id="jumlah_xka">Rp 0,00</p>
                      </div>
                      <br>
                      <div class="row">
                        <p class="col-sm-12">Gol IV Lumpsum : 2 org x 3 hr</p>
                      </div>
                      <div class="row">
                        <div class="col-sm-4">
                          <div class="input-group">
                            <input type="text" class="form-control" id="vol_ukb" name="vol_ukb" placeholder="Volume"  onkeyup="totalHarga('vol_ukb','harga_ukb','jumlah_ukb')">
                            <span class="input-group-addon">OH</span>
                          </div>
                        </div>
                        <p class="col-sm-4" id="harga_ukb">Rp 1.700.000,00</p>
                        <p class="col-sm-4" id="jumlah_ukb">Rp 0,00</p>
                      </div>
                      <br>
                      <div class="row">
                        <p class="col-sm-12">Trasport PP 2org</p>
                      </div>
                      <div class="row">
                        <div class="col-sm-4">
                          <div class="input-group">
                            <input type="text" class="form-control" id="vol_xkb" name="vol_xkb" placeholder="Volume"  onkeyup="totalHarga('vol_xkb','harga_xkb','jumlah_xkb')">
                            <span class="input-group-addon">OK</span>
                          </div>
                        </div>
                        <p class="col-sm-4" id="harga_xkb">Rp 900.000,00</p>
                        <p class="col-sm-4" id="jumlah_xkb">Rp 0,00</p>
                      </div>
                      <br>
                      <div class="row">
                        <p class="col-sm-12">Gol III Lumpsum : 15 org x 3 hr</p>
                      </div>
                      <div class="row">
                        <div class="col-sm-4">
                          <div class="input-group">
                            <input type="text" class="form-control" id="vol_ukc" name="vol_ukc" placeholder="Volume"  onkeyup="totalHarga('vol_ukc','harga_ukc','jumlah_ukc')">
                            <span class="input-group-addon">OH</span>
                          </div>
                        </div>
                        <p class="col-sm-4" id="harga_ukc">Rp 1.500.000,00</p>
                        <p class="col-sm-4" id="jumlah_ukc">Rp 0,00</p>
                      </div>
                      <br>
                      <div class="row">
                        <p class="col-sm-12">Trasport PP 5org</p>
                      </div>
                      <div class="row">
                        <div class="col-sm-4">
                          <div class="input-group">
                            <input type="text" class="form-control" id="vol_xkc" name="vol_xkc" placeholder="Volume"  onkeyup="totalHarga('vol_xkc','harga_xkc','jumlah_xkc')">
                            <span class="input-group-addon">OK</span>
                          </div>
                        </div>
                        <p class="col-sm-4" id="harga_xkc">Rp 900.000,00</p>
                        <p class="col-sm-4" id="jumlah_xkc">Rp 0,00</p>
                      </div>
                      <br>
                      <div class="row">
                        <p class="col-sm-12">Gol III Lumpsum ( peserta Pameran )</p>
                      </div>
                      <div class="row">
                        <div class="col-sm-4">
                          <div class="input-group">
                            <input type="text" class="form-control" id="vol_tkc" name="vol_tkc" placeholder="Volume"  onkeyup="totalHarga('vol_tkc','harga_tkc','jumlah_tkc')">
                            <span class="input-group-addon">OK</span>
                          </div>
                        </div>
                        <p class="col-sm-4" id="harga_tkc">Rp 75.000,00</p>
                        <p class="col-sm-4" id="jumlah_tkc">Rp 0,00</p>
                      </div>
                    </div>
                  </div>
                </div> 
              </div>
              <hr>
              <div class="row">  
                <div class="col-md-6">
                  <div class="form-group">
                    <div class="col-sm-12">
                      <div class="row">
                        <label class="col-sm-12">Pameran TEI </label>
                      </div>
                      <div class="row">
                        <p class="col-sm-12">Gol IV Lumpsum : 1 org x 2 hr</p>
                      </div>
                      <div class="row">
                        <div class="col-sm-4">
                          <div class="input-group">
                            <input type="text" class="form-control" id="vol_hka" name="vol_hka" placeholder="Volume"  onkeyup="totalHarga('vol_hka','harga_hka','jumlah_hka')">
                            <span class="input-group-addon">OH</span>
                          </div>
                        </div>
                        <p class="col-sm-4" id="harga_hka">Rp 1.700.000,00</p>
                        <p class="col-sm-4" id="jumlah_hka">Rp 0,00</p>
                      </div>
                      <br>
                      <div class="row">
                        <p class="col-sm-12">Trasport PP 1org</p>
                      </div>
                      <div class="row">
                        <div class="col-sm-4">
                          <div class="input-group">
                            <input type="text" class="form-control" id="vol_nka" name="vol_nka" placeholder="Volume"  onkeyup="totalHarga('vol_nka','harga_nka','jumlah_nka')">
                            <span class="input-group-addon">OK</span>
                          </div>
                        </div>
                        <p class="col-sm-4" id="harga_nka">Rp 2.700.000,00</p>
                        <p class="col-sm-4" id="jumlah_nka">Rp 0,00</p>
                      </div>
                      <br>
                      <div class="row">
                        <p class="col-sm-12">Gol IV Lumpsum : 1 org x 3 hr</p>
                      </div>
                      <div class="row">
                        <div class="col-sm-4">
                          <div class="input-group">
                            <input type="text" class="form-control" id="vol_hkb" name="vol_hkb" placeholder="Volume"  onkeyup="totalHarga('vol_hkb','harga_hkb','jumlah_hkb')">
                            <span class="input-group-addon">OH</span>
                          </div>
                        </div>
                        <p class="col-sm-4" id="harga_hkb">Rp 1.700.000,00</p>
                        <p class="col-sm-4" id="jumlah_hkb">Rp 0,00</p>
                      </div>
                      <br>
                      <div class="row">
                        <p class="col-sm-12">Trasport PP 1org</p>
                      </div>
                      <div class="row">
                        <div class="col-sm-4">
                          <div class="input-group">
                            <input type="text" class="form-control" id="vol_nkb" name="vol_nkb" placeholder="Volume"  onkeyup="totalHarga('vol_nkb','harga_nkb','jumlah_nkb')">
                            <span class="input-group-addon">OK</span>
                          </div>
                        </div>
                        <p class="col-sm-4" id="harga_nkb">Rp 900.000,00</p>
                        <p class="col-sm-4" id="jumlah_nkb">Rp 0,00</p>
                      </div>
                      <br>
                      <div class="row">
                        <p class="col-sm-12">Gol III Lumpsum : 5 org x 3 hr</p>
                      </div>
                      <div class="row">
                        <div class="col-sm-4">
                          <div class="input-group">
                            <input type="text" class="form-control" id="vol_hkc" name="vol_hkc" placeholder="Volume"  onkeyup="totalHarga('vol_hkc','harga_hkc','jumlah_hkc')">
                            <span class="input-group-addon">OH</span>
                          </div>
                        </div>
                        <p class="col-sm-4" id="harga_hkc">Rp 1.500.000,00</p>
                        <p class="col-sm-4" id="jumlah_hkc">Rp 0,00</p>
                      </div>
                      <br>
                      <div class="row">
                        <p class="col-sm-12">Trasport PP 5org</p>
                      </div>
                      <div class="row">
                        <div class="col-sm-4">
                          <div class="input-group">
                            <input type="text" class="form-control" id="vol_nkc" name="vol_nkc" placeholder="Volume"  onkeyup="totalHarga('vol_nkc','harga_nkc','jumlah_nkc')">
                            <span class="input-group-addon">OK</span>
                          </div>
                        </div>
                        <p class="col-sm-4" id="harga_nkc">Rp 900.000,00</p>
                        <p class="col-sm-4" id="jumlah_nkc">Rp 0,00</p>
                      </div>
                      <br>
                      <div class="row">
                        <p class="col-sm-12">Gol III Lumpsum ( peserta Pameran )</p>
                      </div>
                      <div class="row">
                        <div class="col-sm-4">
                          <div class="input-group">
                            <input type="text" class="form-control" id="vol_1kc" name="vol_1kc" placeholder="Volume"  onkeyup="totalHarga('vol_1kc','harga_1kc','jumlah_1kc')">
                            <span class="input-group-addon">OK</span>
                          </div>
                        </div>
                        <p class="col-sm-4" id="harga_1kc">Rp 75.000,00</p>
                        <p class="col-sm-4" id="jumlah_1kc">Rp 0,00</p>
                      </div> 
                    </div>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <div class="col-sm-12">
                      <div class="row">
                        <label class="col-sm-12">Pameran Jakarta Fair </label>
                      </div>
                      <div class="row">
                        <p class="col-sm-12">Gol IV Lumpsum : 1 org x 2 hr</p>
                      </div>
                      <div class="row">
                        <div class="col-sm-4">
                          <div class="input-group">
                            <input type="text" class="form-control" id="vol_pka" name="vol_pka" placeholder="Volume"  onkeyup="totalHarga('vol_pka','harga_pka','jumlah_pka')">
                            <span class="input-group-addon">OH</span>
                          </div>
                        </div>
                        <p class="col-sm-4" id="harga_pka">Rp 1.700.000,00</p>
                        <p class="col-sm-4" id="jumlah_pka">Rp 0,00</p>
                      </div>
                      <br>
                      <div class="row">
                        <p class="col-sm-12">Trasport PP 1org</p>
                      </div>
                      <div class="row">
                        <div class="col-sm-4">
                          <div class="input-group">
                            <input type="text" class="form-control" id="vol_2ka" name="vol_2ka" placeholder="Volume"  onkeyup="totalHarga('vol_2ka','harga_2ka','jumlah_2ka')">
                            <span class="input-group-addon">OK</span>
                          </div>
                        </div>
                        <p class="col-sm-4" id="harga_2ka">Rp 2.700.000,00</p>
                        <p class="col-sm-4" id="jumlah_2ka">Rp 0,00</p>
                      </div>
                      <br>
                      <div class="row">
                        <p class="col-sm-12">Gol IV Lumpsum : 1 org x 3 hr</p>
                      </div>
                      <div class="row">
                        <div class="col-sm-4">
                          <div class="input-group">
                            <input type="text" class="form-control" id="vol_pkb" name="vol_pkb" placeholder="Volume"  onkeyup="totalHarga('vol_pkb','harga_pkb','jumlah_pkb')">
                            <span class="input-group-addon">OH</span>
                          </div>
                        </div>
                        <p class="col-sm-4" id="harga_pkb">Rp 1.700.000,00</p>
                        <p class="col-sm-4" id="jumlah_pkb">Rp 0,00</p>
                      </div>
                      <br>
                      <div class="row">
                        <p class="col-sm-12">Trasport PP 1org</p>
                      </div>
                      <div class="row">
                        <div class="col-sm-4">
                          <div class="input-group">
                            <input type="text" class="form-control" id="vol_2kb" name="vol_2kb" placeholder="Volume"  onkeyup="totalHarga('vol_2kb','harga_2kb','jumlah_2kb')">
                            <span class="input-group-addon">OK</span>
                          </div>
                        </div>
                        <p class="col-sm-4" id="harga_2kb">Rp 900.000,00</p>
                        <p class="col-sm-4" id="jumlah_2kb">Rp 0,00</p>
                      </div>
                      <br>
                      <div class="row">
                        <p class="col-sm-12">Gol III Lumpsum : 5 org x 3 hr</p>
                      </div>
                      <div class="row">
                        <div class="col-sm-4">
                          <div class="input-group">
                            <input type="text" class="form-control" id="vol_pkc" name="vol_pkc" placeholder="Volume"  onkeyup="totalHarga('vol_pkc','harga_pkc','jumlah_pkc')">
                            <span class="input-group-addon">OH</span>
                          </div>
                        </div>
                        <p class="col-sm-4" id="harga_pkc">Rp 1.500.000,00</p>
                        <p class="col-sm-4" id="jumlah_pkc">Rp 0,00</p>
                      </div>
                      <br>
                      <div class="row">
                        <p class="col-sm-12">Trasport PP 5org</p>
                      </div>
                      <div class="row">
                        <div class="col-sm-4">
                          <div class="input-group">
                            <input type="text" class="form-control" id="vol_2kc" name="vol_2kc" placeholder="Volume"  onkeyup="totalHarga('vol_2kc','harga_2kc','jumlah_2kc')">
                            <span class="input-group-addon">OK</span>
                          </div>
                        </div>
                        <p class="col-sm-4" id="harga_2kc">Rp 900.000,00</p>
                        <p class="col-sm-4" id="jumlah_2kc">Rp 0,00</p>
                      </div>
                    </div>
                  </div>
                </div>     
              </div>
              <hr>  
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <div class="col-sm-12">
                      <div class="row">
                        <label class="col-sm-12">Pameran Fashion & Art Festival Makassar </label>
                      </div>
                      <div class="row">
                        <p class="col-sm-12">Gol IV Lumpsum : 1 org x 2 hr</p>
                      </div>
                      <div class="row">
                        <div class="col-sm-4">
                          <div class="input-group">
                            <input type="text" class="form-control" id="vol_qqa" name="vol_qqa" placeholder="Volume"  onkeyup="totalHarga('vol_qqa','harga_qqa','jumlah_qqa')">
                            <span class="input-group-addon">OH</span>
                          </div>
                        </div>
                        <p class="col-sm-4" id="harga_qqa">Rp 1.700.000,00</p>
                        <p class="col-sm-4" id="jumlah_qqa">Rp 0,00</p>
                      </div>
                      <br>
                      <div class="row">
                        <p class="col-sm-12">Trasport PP 1org</p>
                      </div>
                      <div class="row">
                        <div class="col-sm-4">
                          <div class="input-group">
                            <input type="text" class="form-control" id="vol_xxa" name="vol_xxa" placeholder="Volume"  onkeyup="totalHarga('vol_xxa','harga_xxa','jumlah_xxa')">
                            <span class="input-group-addon">OK</span>
                          </div>
                        </div>
                        <p class="col-sm-4" id="harga_xxa">Rp 2.700.000,00</p>
                        <p class="col-sm-4" id="jumlah_xxa">Rp 0,00</p>
                      </div>
                      <br>
                      <div class="row">
                        <p class="col-sm-12">Gol IV Lumpsum : 1 org x 3 hr</p>
                      </div>
                      <div class="row">
                        <div class="col-sm-4">
                          <div class="input-group">
                            <input type="text" class="form-control" id="vol_qqb" name="vol_qqb" placeholder="Volume"  onkeyup="totalHarga('vol_qqb','harga_qqb','jumlah_qqb')">
                            <span class="input-group-addon">OH</span>
                          </div>
                        </div>
                        <p class="col-sm-4" id="harga_qqb">Rp 1.700.000,00</p>
                        <p class="col-sm-4" id="jumlah_qqb">Rp 0,00</p>
                      </div>
                      <br>
                      <div class="row">
                        <p class="col-sm-12">Trasport PP 1org</p>
                      </div>
                      <div class="row">
                        <div class="col-sm-4">
                          <div class="input-group">
                            <input type="text" class="form-control" id="vol_xxb" name="vol_xxb" placeholder="Volume"  onkeyup="totalHarga('vol_xxb','harga_xxb','jumlah_xxb')">
                            <span class="input-group-addon">OK</span>
                          </div>
                        </div>
                        <p class="col-sm-4" id="harga_xxb">Rp 900.000,00</p>
                        <p class="col-sm-4" id="jumlah_xxb">Rp 0,00</p>
                      </div>
                      <br>
                      <div class="row">
                        <p class="col-sm-12">Gol III Lumpsum : 5 org x 3 hr</p>
                      </div>
                      <div class="row">
                        <div class="col-sm-4">
                          <div class="input-group">
                            <input type="text" class="form-control" id="vol_qqc" name="vol_qqc" placeholder="Volume"  onkeyup="totalHarga('vol_qqc','harga_qqc','jumlah_qqc')">
                            <span class="input-group-addon">OH</span>
                          </div>
                        </div>
                        <p class="col-sm-4" id="harga_qqc">Rp 1.500.000,00</p>
                        <p class="col-sm-4" id="jumlah_qqc">Rp 0,00</p>
                      </div>
                      <br>
                      <div class="row">
                        <p class="col-sm-12">Trasport PP 5org</p>
                      </div>
                      <div class="row">
                        <div class="col-sm-4">
                          <div class="input-group">
                            <input type="text" class="form-control" id="vol_xxc" name="vol_xxc" placeholder="Volume"  onkeyup="totalHarga('vol_xxc','harga_xxc','jumlah_xxc')">
                            <span class="input-group-addon">OK</span>
                          </div>
                        </div>
                        <p class="col-sm-4" id="harga_xxc">Rp 900.000,00</p>
                        <p class="col-sm-4" id="jumlah_xxc">Rp 0,00</p>
                      </div>
                      <br>
                      <div class="row">
                        <p class="col-sm-12">Gol III Lumpsum : 5 org x 5 hr</p>
                      </div>
                      <div class="row">
                        <div class="col-sm-4">
                          <div class="input-group">
                            <input type="text" class="form-control" id="vol_qqd" name="vol_qqd" placeholder="Volume"  onkeyup="totalHarga('vol_qqd','harga_qqd','jumlah_qqd')">
                            <span class="input-group-addon">OH</span>
                          </div>
                        </div>
                        <p class="col-sm-4" id="harga_qqd">Rp 1.000.000,00</p>
                        <p class="col-sm-4" id="jumlah_qqd">Rp 0,00</p>
                      </div>
                      <br>
                      <div class="row">
                        <p class="col-sm-12">Trasport PP 5org</p>
                      </div>
                      <div class="row">
                        <div class="col-sm-4">
                          <div class="input-group">
                            <input type="text" class="form-control" id="vol_xxd" name="vol_xxd" placeholder="Volume"  onkeyup="totalHarga('vol_xxd','harga_xxd','jumlah_xxd')">
                            <span class="input-group-addon">OK</span>
                          </div>
                        </div>
                        <p class="col-sm-4" id="harga_xxd">Rp 900.000,00</p>
                        <p class="col-sm-4" id="jumlah_xxd">Rp 0,00</p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <hr>                
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <div class="col-sm-12">
                      <div class="row">
                         <label class="col-sm-12">Keterangan</label>
                      </div>
                      <div class="row">
                        <div class="col-sm-12">
                          <div class=" input-group">
                            <div class="input-group-addon">
                              <i class="fa fa-file-text"></i>
                            </div>
                            <input type="text" name="keterangan" class="form-control">
                          </div>
                        </div><!-- /.input group -->
                      </div>
                      <br>
                      <div class="row">
                         <label class="col-sm-12">Tanggal Pembahasan</label>
                      </div>
                      <div class="row">
                        <div class="col-sm-12">
                          <div class=" input-group">
                            <div class="input-group-addon">
                              <i class="fa fa-calendar"></i>
                            </div>
                            <input type="text" name="tgl_pembahasan" class="form-control pull-right" id="tgl_pembahasan" value="{{date('d-m-Y')}}">
                          </div>
                        </div><!-- /.input group -->
                      </div>
                      <br>
                      <div class="row">
                         <label class="col-sm-12">Catatan Hasil Pembahasan</label>
                      </div>
                      <div class="row">
                        <div class="col-sm-12">
                          <div class=" input-group">
                            <span class="input-group-addon">1</span>
                            <input type="text" name="catatan_pembahasan1" class="form-control pull-right" id="catatan_pembahasan">
                          </div>
                          <div class=" input-group">
                            <span class="input-group-addon">2</span>
                            <input type="text" name="catatan_pembahasan2" class="form-control pull-right" id="catatan_pembahasan">
                          </div>
                          <div class=" input-group">
                            <span class="input-group-addon">3</span>
                            <input type="text" name="catatan_pembahasan3" class="form-control pull-right" id="catatan_pembahasan">
                          </div>
                        </div><!-- /.input group -->
                      </div>
                    </div>
                  </div><!-- /.form group -->
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <div class="col-sm-12">
                      <div class="row">
                         <label class="col-sm-12">Tim Anggaran Pemerintah Daerah</label>
                      </div>
                      <div class="row">
                        <p class="col-sm-12" >Kepala Bappeda</p>
                      </div>
                      <div class="row">
                        <div class="col-sm-8">
                          <div class=" input-group">
                            <span class="input-group-addon">1</span>
                            <input type="text" name="tim_anggaran1" class="form-control" placeholder="Nama" >
                          </div>
                        </div>
                        <div class="col-sm-4">
                          <input type="text" name="nim_anggaran1" class="form-control" placeholder="NIP" >
                        </div>
                      </div>
                      <br>
                      <div class="row">
                        <p class="col-sm-12" >Kabag Adm.Pembangunan</p>
                      </div>
                      <div class="row">
                        <div class="col-sm-8">
                          <div class=" input-group">
                            <span class="input-group-addon">2</span>
                            <input type="text" name="tim_anggaran2" class="form-control" placeholder="Nama" >
                          </div>
                        </div>
                        <div class="col-sm-4">
                          <input type="text" name="nim_anggaran2" class="form-control" placeholder="NIP" >
                        </div>
                      </div>
                      <br>
                      <div class="row">
                        <p class="col-sm-12" >Kepala DPKAD</p>
                      </div>
                      <div class="row">
                        <div class="col-sm-8">
                          <div class=" input-group">
                            <span class="input-group-addon">3</span>
                            <input type="text" name="tim_anggaran3" class="form-control" placeholder="Nama" >
                          </div>
                        </div>
                        <div class="col-sm-4">
                          <input type="text" name="nim_anggaran3" class="form-control" placeholder="NIP" >
                        </div>
                      </div>
                    </div>
                  </div><!-- /.form group-->
                </div>
              </div>
              <hr>     
              <div class="box-footer">
                <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
                <button type="reset" class="btn btn-warning">Reset</button>
                <button type="submit" class="btn btn-primary">Simpan</butto>
              </div>
            </form>
          </div><!-- /.box-body -->
        </div><!-- /.box -->
      </div><!--/.col (right) -->
    </div>   <!-- /.row -->
  </section><!-- /.content -->
</div><!-- /.content-wrapper -->
@stop
@section('script')
<script>
  $(function () {
    //Initialize Select2 Elements
    $(".select2").select2();

    //Datemask dd/mm/yyyy
    $("#datemask").inputmask("dd/mm/yyyy", {"placeholder": "dd/mm/yyyy"});
    //Datemask2 mm/dd/yyyy
    $("#datemask2").inputmask("mm/dd/yyyy", {"placeholder": "mm/dd/yyyy"});
    //Money Euro
    $("[data-mask]").inputmask();

    //Date range picker
    //$('#reservation').daterangepicker();
    $('#reservation').datepicker({
      maxViewMode:2,
      minViewMode:2,
      format:"yyyy"
    });
    $('#tgl_pembahasan').datepicker({
      format:"dd-mm-yyyy",
      todayHighlight:true
    });
    //Date range picker with time picker
    $('#reservationtime').daterangepicker({timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A'});
    //Date range as a button
    $('#daterange-btn').daterangepicker(
        {
          ranges: {
            'Today': [moment(), moment()],
            'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
            'This Month': [moment().startOf('month'), moment().endOf('month')],
            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
          },
          startDate: moment().subtract(29, 'days'),
          endDate: moment()
        },
    function (start, end) {
      $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
    }
    );

    //iCheck for checkbox and radio inputs
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
      checkboxClass: 'icheckbox_minimal-blue',
      radioClass: 'iradio_minimal-blue'
    });
    //Red color scheme for iCheck
    $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
      checkboxClass: 'icheckbox_minimal-red',
      radioClass: 'iradio_minimal-red'
    });
    //Flat red color scheme for iCheck
    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
      checkboxClass: 'icheckbox_flat-green',
      radioClass: 'iradio_flat-green'
    });

    //Colorpicker
    $(".my-colorpicker1").colorpicker();
    //color picker with addon
    $(".my-colorpicker2").colorpicker();

    //Timepicker
    $(".timepicker").timepicker({
      showInputs: false
    });
  });
</script>
@endsection