@extends('master')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      User Management
      <small></small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="#">Management</a></li>
      <li class="active">User</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <!-- left column -->
      <div class="col-md-4">
        <!-- general form elements disabled -->
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">Tambah User</h3>
          </div><!-- /.box-header -->
          <form class="form" role="form" action="{{Config::get('app.url')}}/manageuser/add" method="post">
            <div class="box-body">
              <div class="form-group">
                <label for="fullname">Nama Lengkap</label>
                <input type="text" class="form-control" id="fullname" name="fullname" placeholder="Masukkan Nama Lengkap" required value="@if(isset($status['input'])){{$status['input']['fullname']}}@endif">
              </div>
              <div class="form-group">
                <label for="username">Username</label>
                <input type="text" class="form-control" id="username" name="username" placeholder="Masukkan Username untuk login" required value="@if(isset($status['input'])){{$status['input']['username']}}@endif">
              </div>
              <div class="form-group">
                <label for="email">E-Mail</label>
                <input type="email" class="form-control" id="email" name="email" placeholder="Masukkan alamat email" required value="@if(isset($status['input'])){{$status['input']['email']}}@endif">
              </div>
              <div class="form-group">
                <label for="pass">Password</label>
                <input type="password" class="form-control" id="pass" name="pass" required value="@if(isset($status['input'])){{$status['input']['pass']}}@endif">
              </div>
              <div class="form-group">
                <label for="pass_con">Konfirmasi Password</label>
                <input type="password" class="form-control" id="pass_con" name="pass_con" required value="@if(isset($status['input'])){{$status['input']['pass_con']}}@endif">
              </div>
              <div class="form-group">
                <label for="permission">Hak Akses Sebagai</label>
                <select class="form-control" id="permission" name="permission">
                  @php($a = '')
                  @php($b = '')
                  @if(isset($status['input']))
                    @if($status['input']['permission']=='admin')
                      @php($a = 'selected')
                      @php($b = '')
                    @else
                      @php($a = '')
                      @php($b = 'selected')
                    @endif
                  @endif
                  <option value="admin" {{$a}}>Admin</option>
                  <option value="user" {{$b}}>User</option>
                </select>
              </div>
            </div><!-- /.box-body -->     
            <div class="box-footer">
              <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
              <button type="reset" class="btn btn-warning"><i class="fa fa-refresh" aria-hidden="true"></i> Reset</button>
              <button type="submit" class="btn btn-primary"><i class="fa fa-plus" aria-hidden="true"></i> Tambah</button>
            </div>
            </form>
          </div><!-- /.box-body -->
        </div><!-- /.box -->
        <div class="col-md-8">
          <div class="box box-success">
                <div class="box-header with-border">
                  <h3 class="box-title">Daftar User</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <table class="table table-bordered" id="example">
                    <thead>
                      <tr>
                        <th>Nama</th>
                        <th>Username</th>
                        <th>Email</th>
                        <th>Role</th>
                        <th>Tindakan</th>
                      </tr>  
                    </thead>
                    <tbody>
                    @foreach($set_data as $user)
                    <tr>
                      <td>{{$user->nama}}</td>
                      <td>{{$user->username}}</td>
                      <td>{{$user->email}}</td>
                      <td><span class="badge bg-red">{{$user->role}}</span></td>
                      <td style="width: 20%">
                        <center>
                        <div class="btn-group">
                          <a href="{{Config::get('app.url')}}/manageuser/edit/{{$user->id}}" class="btn btn-xs btn-success"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>
                          <a href="#" class="btn btn-xs btn-danger" onclick="showModal({{$user->id}})"><i class="fa fa-trash-o" aria-hidden="true"></i> Hapus</button></a>
                        </div>
                        </center>
                      </td>
                    </tr>
                    @endforeach
                    </tbody>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
        </div>
      </div><!--/.col (right) -->
    </div>   <!-- /.row -->
  </section><!-- /.content -->
</div><!-- /.content-wrapper -->
<!-- Modal -->
<div class="modal fade" id="myModalHapus" role="dialog">
  <div class="modal-dialog modal-sm">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Konfirmasi</h4>
      </div>
      <div class="modal-body">
        <p>Apakah anda yakin akan menghapus data ini secara permanen?</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
        <a id="alamat_delete" href="">
          <button class="btn btn-danger"><i class="fa fa-trash" aria-hidden="true"></i> Hapus</button>
        </a>
      </div>
    </div>
  </div>
</div>
@if(isset($status['status']))
<div class="modal fade" id="myModal" role="dialog">
  <div class="modal-dialog modal-sm">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">
        @if($status['status']==true)
        Berhasil
        @else
        Gagal
        @endif</h4>
      </div>
      <div class="modal-body">
        <p>{{$status['msg']}}</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
@endif
@stop
@section('script')
@if(isset($status['status']))
  <script>
    $('#myModal').modal('show');
  </script>
@endif
<script>
  function showModal(id){
      $('#alamat_delete').attr('href',"{{Config::get('app.url')}}/manageuser/delete/" + id);
      $('#myModalHapus').modal('show');
  }
</script>
<script>
  $(document).ready(function() {
      $('#example').DataTable();
  } );
</script>
<script>
  $(function () {
    //Initialize Select2 Elements
    $(".select2").select2();

    //Datemask dd/mm/yyyy
    $("#datemask").inputmask("dd/mm/yyyy", {"placeholder": "dd/mm/yyyy"});
    //Datemask2 mm/dd/yyyy
    $("#datemask2").inputmask("mm/dd/yyyy", {"placeholder": "mm/dd/yyyy"});
    //Money Euro
    $("[data-mask]").inputmask();

    //Date range picker
    //$('#reservation').daterangepicker();
    $('#reservation').datepicker({
      maxViewMode:2,
      minViewMode:2,
      format:"yyyy"
    });
    $('#tgl_pembahasan').datepicker({
      format:"dd-mm-yyyy",
      todayHighlight:true
    });
    //Date range picker with time picker
    $('#reservationtime').daterangepicker({timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A'});
    //Date range as a button
    $('#daterange-btn').daterangepicker(
        {
          ranges: {
            'Today': [moment(), moment()],
            'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
            'This Month': [moment().startOf('month'), moment().endOf('month')],
            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
          },
          startDate: moment().subtract(29, 'days'),
          endDate: moment()
        },
    function (start, end) {
      $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
    }
    );

    //iCheck for checkbox and radio inputs
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
      checkboxClass: 'icheckbox_minimal-blue',
      radioClass: 'iradio_minimal-blue'
    });
    //Red color scheme for iCheck
    $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
      checkboxClass: 'icheckbox_minimal-red',
      radioClass: 'iradio_minimal-red'
    });
    //Flat red color scheme for iCheck
    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
      checkboxClass: 'icheckbox_flat-green',
      radioClass: 'iradio_flat-green'
    });

    //Colorpicker
    $(".my-colorpicker1").colorpicker();
    //color picker with addon
    $(".my-colorpicker2").colorpicker();

    //Timepicker
    $(".timepicker").timepicker({
      showInputs: false
    });
  });
</script>
@endsection