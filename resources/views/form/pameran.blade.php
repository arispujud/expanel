@extends('master')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Pameran
      <small></small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="#">Forms</a></li>
      <li class="active">Pameran</li>
    </ol>
  </section>

  <section class="content">
    <div class="row">
      <!-- left column -->
      <div class="col-md-12">
        <!-- general form elements disabled -->
        <div class="panel panel-primary">
          <div class="panel-heading"><b>Form Isian</b></div>
          <form class="form-horizontal" role="form" action="{{Config::get('app.url')}}/pameran" method="post">
            <div class="panel-body">
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="col-sm-3 control-label">Formulir</label>
                    <div class="col-sm-8 input-group">
                      <div class="input-group-addon">
                        <i class="fa fa-file-text"></i>
                      </div>
                      <input type="text" name="formulir" class="form-control"  value="RKA-SKPD 2.2.1">
                    </div><!-- /.input group -->
                  </div><!-- /.form group -->
                  <div class="form-group">
                    <label class="col-sm-3 control-label">Tahun Anggaran</label>
                    <div class="col-sm-8 input-group">
                      <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                      </div>
                      <input type="text" name="thn_anggaran" class="form-control pull-right" id="reservation" value="{{date('Y')}}">
                    </div><!-- /.input group -->
                  </div><!-- /.form group -->
                  <div class="form-group">
                    <label class="col-sm-3 control-label">Lokasi Kegiatan</label>
                    <div class="col-sm-8 input-group">
                      <div class="input-group-addon">
                        <i class="fa fa-map-marker"></i>
                      </div>
                      <input type="text" name="lokasi_kegiatan" class="form-control"  value="Kabupaten Bantul">
                    </div><!-- /.input group -->
                  </div><!-- /.form group -->
                  <div class="form-group">
                    <label class="col-sm-3 control-label">Sasaran Kegiatan</label>
                    <div class="col-sm-8 input-group">
                      <div class="input-group-addon">
                        <i class="fa fa-dot-circle-o"></i>
                      </div>
                      <input type="text" name="sasaran" class="form-control">
                    </div><!-- /.input group -->
                  </div><!-- /.form group -->
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="col-sm-3 control-label">Capaian Program</label>
                    <div class="col-sm-8 input-group">
                      <div class="input-group-addon">
                        <i class="fa fa-star"></i>
                      </div>
                      <input type="text" name="capaian" class="form-control">
                    </div><!-- /.input group -->
                  </div><!-- /.form group -->
                  <div class="form-group">
                    <label class="col-sm-3 control-label">Masukkan</label>
                    <div class="col-sm-8 input-group">
                      <div class="input-group-addon">
                        <i class="fa fa-download"></i>
                      </div>
                      <input type="text" name="masukkan" class="form-control">
                    </div><!-- /.input group -->
                  </div><!-- /.form group -->
                  <div class="form-group">
                    <label class="col-sm-3 control-label">Keluaran</label>
                    <div class="col-sm-8 input-group">
                      <div class="input-group-addon">
                        <i class="fa fa-upload"></i>
                      </div>
                      <input type="text" name="keluaran" class="form-control">
                    </div><!-- /.input group -->
                  </div><!-- /.form group -->
                  <div class="form-group">
                    <label class="col-sm-3 control-label">Hasil</label>
                    <div class="col-sm-8 input-group">
                      <div class="input-group-addon">
                        <i class="fa fa-thumbs-up"></i>
                      </div>
                      <input type="text" name="hasil" class="form-control">
                    </div><!-- /.input group -->
                  </div><!-- /.form group -->
                </div>
              </div>
              <hr>
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <div class="col-sm-12">
                      <div class="row">
                        <label class="col-sm-12">Honor Tim Pengadaan Barang dan Jasa</label>
                      </div>
                      <div class="row">
                        <p class="col-sm-12">Honor Pejabat Pembuat Komitmen</p>
                      </div>
                      <div class="row">
                        <div class="col-sm-4">
                          <div class="input-group">
                            <input type="text" class="form-control" id="vol_hppk" name="vol_hppk" placeholder="Volume"  onkeyup="totalHarga('vol_hppk','harga_hppk','jumlah_hppk')">
                            <span class="input-group-addon">OP</span>
                          </div>
                        </div>
                        <p class="col-sm-4" id="harga_hppk">Rp 225.000,00</p>
                        <p class="col-sm-4" id="jumlah_hppk">Rp 0,00</p>
                      </div>
                      <br>
                      <div class="row">
                        <p class="col-sm-12">Honor Pejabat Pengadaan</p>
                      </div>
                      <div class="row">
                        <div class="col-sm-4">
                          <div class="input-group">
                            <input type="text" class="form-control" id="vol_hpp" name="vol_hpp" placeholder="Volume"  onkeyup="totalHarga('vol_hpp','harga_hpp','jumlah_hpp')">
                            <span class="input-group-addon">OP</span>
                          </div>
                        </div>
                        <p class="col-sm-4" id="harga_hpp">Rp 175.000,00</p>
                        <p class="col-sm-4" id="jumlah_hpp">Rp 0,00</p>
                      </div>
                      <br>
                      <div class="row">
                        <p class="col-sm-12">Honor Panitia Penerima/Pemeriksa Hasil Kerja</p>
                      </div>
                      <div class="row">
                        <div class="col-sm-4">
                          <div class="input-group">
                            <input type="text" class="form-control" id="vol_hasil" name="vol_hasil" placeholder="Volume"  onkeyup="totalHarga('vol_hasil','harga_hasil','jumlah_hasil')">
                            <span class="input-group-addon">OP</span>
                          </div>
                        </div>
                        <p class="col-sm-4" id="harga_hasil">Rp 360.000,00</p>
                        <p class="col-sm-4" id="jumlah_hasil">Rp 0,00</p>
                      </div>
                    </div>
                  </div> 
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <div class="col-sm-12">
                      <div class="row">
                        <label class="col-sm-12">Belanja Sewa Mobilitas</label>
                      </div>
                      <div class="row">
                        <p class="col-sm-12">Belanja Sarana Mobilitas Darat</p>
                      </div>
                      <div class="row">
                        <div class="col-sm-4">
                          <div class="input-group">
                            <input type="text" class="form-control" id="vol_bsm" name="vol_bsm" placeholder="Volume"  onkeyup="totalHarga('vol_bsm','harga_bsm','jumlah_bsm')">
                            <span class="input-group-addon">LS</span>
                          </div>
                        </div>
                        <p class="col-sm-4" id="harga_bsm">Rp 10.000,00</p>
                        <p class="col-sm-4" id="jumlah_bsm">Rp 0,00</p>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <div class="col-sm-12">
                      <div class="row">
                        <label class="col-sm-12">Belanja Sewa Perlengkapan dan Peralatan Kantor</label>
                      </div>
                      <div class="row">
                        <p class="col-sm-12">Belanja Sewa Peralatan Display</p>
                      </div>
                      <div class="row">
                        <div class="col-sm-4">
                          <div class="input-group">
                            <input type="text" class="form-control" id="vol_bsp" name="vol_bsp" placeholder="Volume"  onkeyup="totalHarga('vol_bsp','harga_bsp','jumlah_bsp')">
                            <span class="input-group-addon">KGT</span>
                          </div>
                        </div>
                        <p class="col-sm-4" id="harga_bsp">Rp 20.000,00</p>
                        <p class="col-sm-4" id="jumlah_bsp">Rp 0,00</p>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <div class="col-sm-12">
                      <div class="row">
                        <label class="col-sm-12">Biaya Makan dan Minum</label>
                      </div>
                      <div class="row">
                        <p class="col-sm-12">Biaya Makan dan Minum Rapat</p>
                      </div>
                      <div class="row">
                        <div class="col-sm-4">
                          <div class="input-group">
                            <input type="text" class="form-control" id="vol_makan" name="vol_makan" placeholder="Volume"  onkeyup="totalHarga('vol_makan','harga_makan','jumlah_makan')">
                            <span class="input-group-addon">OH</span>
                          </div>
                        </div>
                        <p class="col-sm-4" id="harga_makan">Rp 800,00</p>
                        <p class="col-sm-4" id="jumlah_makan">Rp 0,00</p>
                      </div>
                    </div>
                  </div>
                </div>      
              </div>
              <hr>
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <div class="col-sm-12">
                      <div class="row">
                        <label class="col-sm-12">Belanja Barang Habis Pakai</label>
                      </div>
                      <div class="row">
                        <p class="col-sm-12">Belanja Saran Komputer</p>
                      </div>
                      <div class="row">
                        <div class="col-sm-4">
                          <div class="input-group">
                            <input type="text" class="form-control" id="vol_bsk" name="vol_bsk" placeholder="Volume"  onkeyup="totalHarga('vol_bsk','harga_bsk','jumlah_bsk')">
                            <span class="input-group-addon">kgt</span>
                          </div>
                        </div>
                        <p class="col-sm-4" id="harga_bsk">Rp 500.000,00</p>
                        <p class="col-sm-4" id="jumlah_bsk">Rp 000,00</p>
                      </div>
                      <br>
                      <div class="row">
                        <p class="col-sm-12">Belanja Alat Tulis</p>
                      </div>
                      <div class="row">
                        <div class="col-sm-4">
                          <div class="input-group">
                            <input type="text" class="form-control" id="vol_bat" name="vol_bat" placeholder="Volume"  onkeyup="totalHarga('vol_bat','harga_bat','jumlah_bat')">
                            <span class="input-group-addon">kgt</span>
                          </div>
                        </div>
                        <p class="col-sm-4" id="harga_bat">Rp 500.000,00</p>
                        <p class="col-sm-4" id="jumlah_bat">Rp 0,00</p>
                      </div>
                      <br>
                      <div class="row">
                        <label class="col-sm-12">Belanja Bahan/ Material</label>
                      </div>
                      <div class="row">
                        <p class="col-sm-12">Belanja Souvenir</p>
                      </div>
                      <div class="row">
                        <div class="col-sm-4">
                          <div class="input-group">
                            <input type="text" class="form-control" id="vol_svr" name="vol_svr" placeholder="Volume"  onkeyup="totalHarga('vol_svr','harga_svr','jumlah_svr')">
                            <span class="input-group-addon">kgt</span>
                          </div>
                        </div>
                        <p class="col-sm-4" id="harga_svr">Rp 1.000.000,00</p>
                        <p class="col-sm-4" id="jumlah_svr">Rp 000,00</p>
                      </div>
                      <br>
                     <div class="row">
                        <label class="col-sm-12">Belanja Jasa Kantor</label>
                      </div>
                      <div class="row">
                        <p class="col-sm-12">Belanja Dekorasi</p>
                      </div>
                      <div class="row">
                        <div class="col-sm-4">
                          <div class="input-group">
                            <input type="text" class="form-control" id="vol_dks" name="vol_dks" placeholder="Volume"  onkeyup="totalHarga('vol_dks','harga_dks','jumlah_dks')">
                            <span class="input-group-addon">kgt</span>
                          </div>
                        </div>
                        <p class="col-sm-4" id="harga_dks">Rp 300.000,00</p>
                        <p class="col-sm-4" id="jumlah_dks">Rp 0,00</p>
                      </div>
                      <br>
                    </div>
                  </div>
                </div> 
                <div class="col-md-6">
                  <div class="form-group">
                    <div class="col-sm-12">
                      <div class="row">
                        <label class="col-sm-12">Belanja Perjalanan Dinas</label>
                      </div>
                      <div class="row">
                        <p class="col-sm-12">Gol IV "Lumpsum" 4org x 2hr x 1 kgt</p>
                      </div>
                      <div class="row">
                        <div class="col-sm-4">
                          <div class="input-group">
                            <input type="text" class="form-control" id="vol_lps" name="vol_lps" placeholder="Volume"  onkeyup="totalHarga('vol_lps','harga_lps','jumlah_lps')">
                            <span class="input-group-addon">OH</span>
                          </div>
                        </div>
                        <p class="col-sm-4" id="harga_lps">Rp 1.700.000,00</p>
                        <p class="col-sm-4" id="jumlah_lps">Rp 0,00</p>
                      </div>
                      <br>
                      <div class="row">
                        <p class="col-sm-12">Gol IV "Transport" 4org x 1 kgt</p>
                      </div>
                      <div class="row">
                        <div class="col-sm-4">
                          <div class="input-group">
                            <input type="text" class="form-control" id="vol_tpt" name="vol_tpt" placeholder="Volume"  onkeyup="totalHarga('vol_tpt','harga_tpt','jumlah_tpt')">
                            <span class="input-group-addon">OK</span>
                          </div>
                        </div>
                        <p class="col-sm-4" id="harga_tpt">Rp 1.500.000,00</p>
                        <p class="col-sm-4" id="jumlah_tpt">Rp 0,00</p>
                      </div>
                      <br>
                      <div class="row">
                        <p class="col-sm-12">Gol IV "Lumpsum" 3org x 3hr x 1kgt</p>
                      </div>
                      <div class="row">
                        <div class="col-sm-4">
                          <div class="input-group">
                            <input type="text" class="form-control" id="vol_lmm" name="vol_lmm" placeholder="Volume"  onkeyup="totalHarga('vol_lmm','harga_lmm','jumlah_lmm')">
                            <span class="input-group-addon">OH</span>
                          </div>
                        </div>
                        <p class="col-sm-4" id="harga_lmm">Rp 1.700.000,00</p>
                        <p class="col-sm-4" id="jumlah_lmm">Rp 0,00</p>
                      </div>
                      <br>
                      <div class="row">
                        <p class="col-sm-12">Gol IV "Trasport" 3org x 1kgt</p>
                      </div>
                      <div class="row">
                        <div class="col-sm-4">
                          <div class="input-group">
                            <input type="text" class="form-control" id="vol_trt" name="vol_trt" placeholder="Volume"  onkeyup="totalHarga('vol_trt','harga_trt','jumlah_trt')">
                            <span class="input-group-addon">OH</span>
                          </div>
                        </div>
                        <p class="col-sm-4" id="harga_trt">Rp 1.500.000,00</p>
                        <p class="col-sm-4" id="jumlah_trt">Rp 0,00</p>
                      </div>
                    </div>
                  </div> 
                </div>
              </div>  
              <hr>
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <div class="col-sm-12">
                      <div class="row">
                        <label class="col-sm-12">Belanja Cetak Pengadaan</label>
                      </div>
                      <div class="row">
                        <p class="col-sm-12">Belanja Cetak Leaflet</p>
                      </div>
                      <div class="row">
                        <div class="col-sm-4">
                          <div class="input-group">
                            <input type="text" class="form-control" id="vol_bcl" name="vol_bcl" placeholder="Volume"  onkeyup="totalHarga('vol_bcl','harga_bcl','jumlah_bcl')">
                            <span class="input-group-addon">EKS</span>
                          </div>
                        </div>
                        <p class="col-sm-4" id="harga_bcl">Rp 50.000,00</p>
                        <p class="col-sm-4" id="jumlah_bcl">Rp 0,00</p>
                      </div>
                      <br>
                      <div class="row">
                        <p class="col-sm-12">Belanja Cetak Bahan Promosi</p>
                      </div>
                      <div class="row">
                        <div class="col-sm-4">
                          <div class="input-group">
                            <input type="text" class="form-control" id="vol_bcb" name="vol_bcb" placeholder="Volume"  onkeyup="totalHarga('vol_bcb','harga_bcb','jumlah_bcb')">
                            <span class="input-group-addon">EKS</span>
                          </div>
                        </div>
                        <p class="col-sm-4" id="harga_bcb">Rp 500.000,00</p>
                        <p class="col-sm-4" id="jumlah_bcb">Rp 0,00</p>
                      </div>
                      <br>
                      <div class="row">
                        <p class="col-sm-12">Belanja Foto Copy</p>
                      </div>
                      <div class="row">
                        <div class="col-sm-4">
                          <div class="input-group">
                            <input type="text" class="form-control" id="vol_bfc" name="vol_bfc" placeholder="Volume"  onkeyup="totalHarga('vol_bfc','harga_bfc','jumlah_bfc')">
                            <span class="input-group-addon">LBR</span>
                          </div>
                        </div>
                        <p class="col-sm-4" id="harga_bfc">Rp 150,00</p>
                        <p class="col-sm-4" id="jumlah_bfc">Rp 0,00</p>
                      </div>
                      <br>
                      <div class="row">
                        <label class="col-sm-12">Honorium Pelaksanaan Kegiatan</label>
                      </div>
                      <div class="row">
                        <p class="col-sm-12">Uang Saku Peserta</p>
                      </div>
                      <div class="row">
                        <div class="col-sm-4">
                          <div class="input-group">
                            <input type="text" class="form-control" id="vol_usp" name="vol_usp" placeholder="Volume"  onkeyup="totalHarga('vol_usp','harga_usp','jumlah_usp')">
                            <span class="input-group-addon">LBR</span>
                          </div>
                        </div>
                        <p class="col-sm-4" id="harga_usp">Rp 65.000,00</p>
                        <p class="col-sm-4" id="jumlah_usp">Rp 0,00</p>
                      </div>
                    </div>
                  </div> 
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <div class="col-sm-12">
                      <div class="row">
                        <label class="col-sm-12">Belanja Perjalanan Dinas</label>
                      </div>
                      <div class="row">
                        <p class="col-sm-12">Gol III "Lumpsum" 7org x 3hr x 1 kgt</p>
                      </div>
                      <div class="row">
                        <div class="col-sm-4">
                          <div class="input-group">
                            <input type="text" class="form-control" id="vol_lum" name="vol_lum" placeholder="Volume"  onkeyup="totalHarga('vol_lum','harga_lum','jumlah_lum')">
                            <span class="input-group-addon">OH</span>
                          </div>
                        </div>
                        <p class="col-sm-4" id="harga_lum">Rp 1.550.000,00</p>
                        <p class="col-sm-4" id="jumlah_lum">Rp 0,00</p>
                      </div>
                      <br>
                      <div class="row">
                        <p class="col-sm-12">Gol III "Transport" 7org x 1 kgt</p>
                      </div>
                      <div class="row">
                        <div class="col-sm-4">
                          <div class="input-group">
                            <input type="text" class="form-control" id="vol_tra" name="vol_tra" placeholder="Volume"  onkeyup="totalHarga('vol_tra','harga_tra','jumlah_tra')">
                            <span class="input-group-addon">OK</span>
                          </div>
                        </div>
                        <p class="col-sm-4" id="harga_tra">Rp 1.500.000,00</p>
                        <p class="col-sm-4" id="jumlah_tra">Rp 0,00</p>
                      </div>
                      <br>
                      <div class="row">
                        <p class="col-sm-12">Gol III "Lumpsum" 4org x 3hr x 1kgt</p>
                      </div>
                      <div class="row">
                        <div class="col-sm-4">
                          <div class="input-group">
                            <input type="text" class="form-control" id="vol_lms" name="vol_lms" placeholder="Volume"  onkeyup="totalHarga('vol_lms','harga_lms','jumlah_lms')">
                            <span class="input-group-addon">OH</span>
                          </div>
                        </div>
                        <p class="col-sm-4" id="harga_lms">Rp 1.700.000,00</p>
                        <p class="col-sm-4" id="jumlah_lms">Rp 0,00</p>
                      </div>
                      <br>
                      <div class="row">
                        <p class="col-sm-12">Gol III "Trasport" 3org x 1kgt</p>
                      </div>
                      <div class="row">
                        <div class="col-sm-4">
                          <div class="input-group">
                            <input type="text" class="form-control" id="vol_prt" name="vol_prt" placeholder="Volume"  onkeyup="totalHarga('vol_prt','harga_prt','jumlah_prt')">
                            <span class="input-group-addon">OH</span>
                          </div>
                        </div>
                        <p class="col-sm-4" id="harga_prt">Rp 1.500.000,00</p>
                        <p class="col-sm-4" id="jumlah_prt">Rp 0,00</p>
                      </div>
                    </div>
                  </div> 
                </div>
              </div> 
              <hr>          
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <div class="col-sm-12">
                      <div class="row">
                         <label class="col-sm-12">Keterangan</label>
                      </div>
                      <div class="row">
                        <div class="col-sm-12">
                          <div class=" input-group">
                            <div class="input-group-addon">
                              <i class="fa fa-file-text"></i>
                            </div>
                            <input type="text" name="keterangan" class="form-control">
                          </div>
                        </div><!-- /.input group -->
                      </div>
                      <br>
                      <div class="row">
                         <label class="col-sm-12">Tanggal Pembahasan</label>
                      </div>
                      <div class="row">
                        <div class="col-sm-12">
                          <div class=" input-group">
                            <div class="input-group-addon">
                              <i class="fa fa-calendar"></i>
                            </div>
                            <input type="text" name="tgl_pembahasan" class="form-control pull-right" id="tgl_pembahasan" value="{{date('d-m-Y')}}">
                          </div>
                        </div><!-- /.input group -->
                      </div>
                      <br>
                      <div class="row">
                         <label class="col-sm-12">Catatan Hasil Pembahasan</label>
                      </div>
                      <div class="row">
                        <div class="col-sm-12">
                          <div class=" input-group">
                            <span class="input-group-addon">1</span>
                            <input type="text" name="catatan_pembahasan1" class="form-control pull-right" id="catatan_pembahasan">
                          </div>
                          <div class=" input-group">
                            <span class="input-group-addon">2</span>
                            <input type="text" name="catatan_pembahasan2" class="form-control pull-right" id="catatan_pembahasan">
                          </div>
                          <div class=" input-group">
                            <span class="input-group-addon">3</span>
                            <input type="text" name="catatan_pembahasan3" class="form-control pull-right" id="catatan_pembahasan">
                          </div>
                        </div><!-- /.input group -->
                      </div>
                    </div>
                  </div><!-- /.form group -->
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <div class="col-sm-12">
                      <div class="row">
                         <label class="col-sm-12">Tim Anggaran Pemerintah Daerah</label>
                      </div>
                      <div class="row">
                        <p class="col-sm-12" >Kepala Bappeda</p>
                      </div>
                      <div class="row">
                        <div class="col-sm-8">
                          <div class=" input-group">
                            <span class="input-group-addon">1</span>
                            <input type="text" name="tim_anggaran1" class="form-control" placeholder="Nama" >
                          </div>
                        </div>
                        <div class="col-sm-4">
                          <input type="text" name="nim_anggaran1" class="form-control" placeholder="NIP" >
                        </div>
                      </div>
                      <br>
                      <div class="row">
                        <p class="col-sm-12" >Kabag Adm.Pembangunan</p>
                      </div>
                      <div class="row">
                        <div class="col-sm-8">
                          <div class=" input-group">
                            <span class="input-group-addon">2</span>
                            <input type="text" name="tim_anggaran2" class="form-control" placeholder="Nama" >
                          </div>
                        </div>
                        <div class="col-sm-4">
                          <input type="text" name="nim_anggaran2" class="form-control" placeholder="NIP" >
                        </div>
                      </div>
                      <br>
                      <div class="row">
                        <p class="col-sm-12" >Kepala DPKAD</p>
                      </div>
                      <div class="row">
                        <div class="col-sm-8">
                          <div class=" input-group">
                            <span class="input-group-addon">3</span>
                            <input type="text" name="tim_anggaran3" class="form-control" placeholder="Nama" >
                          </div>
                        </div>
                        <div class="col-sm-4">
                          <input type="text" name="nim_anggaran3" class="form-control" placeholder="NIP" >
                        </div>
                      </div>
                    </div>
                  </div><!-- /.form group-->
                </div>
              </div>
              <hr>     
              <div class="box-footer">
                <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
                <button type="reset" class="btn btn-warning">Reset</button>
                <button type="submit" class="btn btn-primary">Simpan</butto>
              </div>
            </form>
          </div><!-- /.box-body -->
        </div><!-- /.box -->
      </div><!--/.col (right) -->
    </div>   <!-- /.row -->
  </section><!-- /.content -->
</div><!-- /.content-wrapper -->
@stop
@section('script')
<script>
  $(function () {
    //Initialize Select2 Elements
    $(".select2").select2();

    //Datemask dd/mm/yyyy
    $("#datemask").inputmask("dd/mm/yyyy", {"placeholder": "dd/mm/yyyy"});
    //Datemask2 mm/dd/yyyy
    $("#datemask2").inputmask("mm/dd/yyyy", {"placeholder": "mm/dd/yyyy"});
    //Money Euro
    $("[data-mask]").inputmask();

    //Date range picker
    //$('#reservation').daterangepicker();
    $('#reservation').datepicker({
      maxViewMode:2,
      minViewMode:2,
      format:"yyyy"
    });
    $('#tgl_pembahasan').datepicker({
      format:"dd-mm-yyyy",
      todayHighlight:true
    });
    //Date range picker with time picker
    $('#reservationtime').daterangepicker({timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A'});
    //Date range as a button
    $('#daterange-btn').daterangepicker(
        {
          ranges: {
            'Today': [moment(), moment()],
            'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
            'This Month': [moment().startOf('month'), moment().endOf('month')],
            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
          },
          startDate: moment().subtract(29, 'days'),
          endDate: moment()
        },
    function (start, end) {
      $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
    }
    );

    //iCheck for checkbox and radio inputs
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
      checkboxClass: 'icheckbox_minimal-blue',
      radioClass: 'iradio_minimal-blue'
    });
    //Red color scheme for iCheck
    $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
      checkboxClass: 'icheckbox_minimal-red',
      radioClass: 'iradio_minimal-red'
    });
    //Flat red color scheme for iCheck
    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
      checkboxClass: 'icheckbox_flat-green',
      radioClass: 'iradio_flat-green'
    });

    //Colorpicker
    $(".my-colorpicker1").colorpicker();
    //color picker with addon
    $(".my-colorpicker2").colorpicker();

    //Timepicker
    $(".timepicker").timepicker({
      showInputs: false
    });
  });
</script>
@endsection