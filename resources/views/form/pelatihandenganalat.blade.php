@extends('master')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Pelatihan Dengan Alat
      <small></small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="#">Forms</a></li>
      <li class="active">Pelatihan dengan Alat</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <!-- left column -->
      <div class="col-md-12">
        <!-- general form elements disabled -->
        <div class="panel panel-primary">
          <div class="panel-heading"><b>Form Isian</b>
          </div><!-- /.box-header -->
          <form class="form-horizontal" role="form" action="{{Config::get('app.url')}}/pelatihandenganalat" method="post">
            <div class="box-body">
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="col-sm-3 control-label">Formulir</label>
                    <div class="col-sm-8 input-group">
                      <div class="input-group-addon">
                        <i class="fa fa-file-text"></i>
                      </div>
                      <input type="text" name="formulir" class="form-control"  value="RKA-SKPD 2.2.1">
                    </div><!-- /.input group -->
                  </div><!-- /.form group -->
                  <div class="form-group">
                    <label class="col-sm-3 control-label">Tahun Anggaran</label>
                    <div class="col-sm-8 input-group">
                      <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                      </div>
                      <input type="text" name="thn_anggaran" class="form-control pull-right" id="reservation" value="{{date('Y')}}">
                    </div><!-- /.input group -->
                  </div><!-- /.form group -->
                  <div class="form-group">
                    <label class="col-sm-3 control-label">Lokasi Kegiatan</label>
                    <div class="col-sm-8 input-group">
                      <div class="input-group-addon">
                        <i class="fa fa-map-marker"></i>
                      </div>
                      <input type="text" name="lokasi_kegiatan" class="form-control"  value="Kabupaten Bantul">
                    </div><!-- /.input group -->
                  </div><!-- /.form group -->
                  <div class="form-group">
                    <label class="col-sm-3 control-label">Sasaran Kegiatan</label>
                    <div class="col-sm-8 input-group">
                      <div class="input-group-addon">
                        <i class="fa fa-dot-circle-o"></i>
                      </div>
                      <input type="text" name="sasaran" class="form-control">
                    </div><!-- /.input group -->
                  </div><!-- /.form group -->
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="col-sm-3 control-label">Capaian Program</label>
                    <div class="col-sm-8 input-group">
                      <div class="input-group-addon">
                        <i class="fa fa-star"></i>
                      </div>
                      <input type="text" name="capaian" class="form-control">
                    </div><!-- /.input group -->
                  </div><!-- /.form group -->
                  <div class="form-group">
                    <label class="col-sm-3 control-label">Masukkan</label>
                    <div class="col-sm-8 input-group">
                      <div class="input-group-addon">
                        <i class="fa fa-download"></i>
                      </div>
                      <input type="text" name="masukkan" class="form-control">
                    </div><!-- /.input group -->
                  </div><!-- /.form group -->
                  <div class="form-group">
                    <label class="col-sm-3 control-label">Keluaran</label>
                    <div class="col-sm-8 input-group">
                      <div class="input-group-addon">
                        <i class="fa fa-upload"></i>
                      </div>
                      <input type="text" name="keluaran" class="form-control">
                    </div><!-- /.input group -->
                  </div><!-- /.form group -->
                  <div class="form-group">
                    <label class="col-sm-3 control-label">Hasil</label>
                    <div class="col-sm-8 input-group">
                      <div class="input-group-addon">
                        <i class="fa fa-thumbs-up"></i>
                      </div>
                      <input type="text" name="hasil" class="form-control">
                    </div><!-- /.input group -->
                  </div><!-- /.form group -->
                </div>
              </div>
              <hr>
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <div class="col-sm-12">
                      <div class="row">
                        <label class="col-sm-12">Honor Tim Pengadaan Barang & Jasa</label>
                      </div>
                      <div class="row">
                        <p class="col-sm-12">Honor Pejabat Pembuat Komitmen</p>
                      </div>
                      <div class="row">
                        <div class="col-sm-4">
                          <div class="input-group">
                            <input type="text" name="vol_honor_tim1" id="vol_honor_tim1" class="form-control" placeholder="volume" onkeyup="totalHarga('vol_honor_tim1','harga_honor_tim1','jumlah_honor_tim1')">
                            <span class="input-group-addon">OP</span>
                          </div>
                        </div>
                        <p class="col-sm-4" id="harga_honor_tim1">Rp 250.000,00</p>
                        <p class="col-sm-4" id="jumlah_honor_tim1">Rp 0,00</p>
                      </div>
                      <br>
                      <div class="row">
                        <p class="col-sm-12">Honor Pejabat Pengadaan</p>
                      </div>
                      <div class="row">
                        <div class="col-sm-4">
                          <div class="input-group">
                            <input type="text" name="vol_honor_tim2" id="vol_honor_tim2" class="form-control" placeholder="volume" onkeyup="totalHarga('vol_honor_tim2','harga_honor_tim2','jumlah_honor_tim2')">
                            <span class="input-group-addon">OP</span>
                          </div>
                        </div>
                        <p class="col-sm-4" id="harga_honor_tim2">Rp 200.000,00</p>
                        <p class="col-sm-4" id="jumlah_honor_tim2">Rp 0,00</p>
                      </div>
                      <br>
                      <div class="row">
                        <p class="col-sm-12">Honor Panitia Penerima/Pemeriksa Hasil Pekerjaan</p>
                      </div>
                      <div class="row">
                        <div class="col-sm-4">
                          <div class="input-group">
                            <input type="text" name="vol_honor_tim3" id="vol_honor_tim3" class="form-control" placeholder="volume" onkeyup="totalHarga('vol_honor_tim3','harga_honor_tim3','jumlah_honor_tim3')">
                            <span class="input-group-addon">OP</span>
                          </div>
                        </div>
                        <p class="col-sm-4" id="harga_honor_tim3">Rp 360.000,00</p>
                        <p class="col-sm-4" id="jumlah_honor_tim3">Rp 0,00</p>
                      </div>
                    </div>
                  </div><!-- /.form group -->
                  <hr>
                  <div class="form-group">
                    <div class="col-sm-12">
                      <div class="row">
                        <label class="col-sm-12">Belanja Bahan Habis Pakai</label>
                      </div>
                      <div class="row">
                        <p class="col-sm-12">Belanja ATK Pelatihan</p>
                      </div>
                      <div class="row">
                        <div class="col-sm-4">
                          <div class="input-group">
                            <input type="text" name="vol_atk1" id="vol_atk1" class="form-control" placeholder="volume" onkeyup="totalHarga('vol_atk1','harga_atk1','jumlah_atk1')">
                            <span class="input-group-addon">kgt</span>
                          </div>
                        </div>
                        <p class="col-sm-4" id="harga_atk1">Rp 500.000,00</p>
                        <p class="col-sm-4" id="jumlah_atk1">Rp 0,00</p>
                      </div>
                      <br>
                      <div class="row">
                        <p class="col-sm-12">Belanja ATK Peserta Pelatihan</p>
                      </div>
                      <div class="row">
                        <div class="col-sm-4">
                          <div class="input-group">
                            <input type="text" name="vol_atk2" id="vol_atk2" class="form-control" placeholder="volume" onkeyup="totalHarga('vol_atk2','harga_atk2','jumlah_atk2')">
                            <span class="input-group-addon">paket</span>
                          </div>
                        </div>
                        <p class="col-sm-4" id="harga_atk2">Rp 65.000,00</p>
                        <p class="col-sm-4" id="jumlah_atk2">Rp 0,00</p>
                      </div>
                      <br>
                      <div class="row">
                        <p class="col-sm-12">Belanja Atribut Praktek Lapangan</p>
                      </div>
                      <div class="row">
                        <div class="col-sm-4">
                          <div class="input-group">
                            <input type="text" name="vol_atk3" id="vol_atk3" class="form-control" placeholder="volume" onkeyup="totalHarga('vol_atk3','harga_atk3','jumlah_atk3')">
                            <span class="input-group-addon">peserta</span>
                          </div>
                        </div>
                        <p class="col-sm-4" id="harga_atk3">Rp 150.000,00</p>
                        <p class="col-sm-4" id="jumlah_atk3">Rp 0,00</p>
                      </div>
                    </div>
                  </div><!-- /.form group -->
                  <hr>
                  <div class="form-group">
                    <div class="col-sm-12">
                      <div class="row">
                        <label class="col-sm-12">Belanja Bahan/Material</label>
                      </div>
                      <div class="row">
                        <p class="col-sm-12">Belanja Bahan Percontohan</p>
                      </div>
                      <div class="row">
                        <div class="col-sm-4">
                          <div class="input-group">
                            <input type="text" name="vol_bm1" id="vol_bm1" class="form-control" placeholder="volume" onkeyup="totalHarga('vol_bm1','harga_bm1','jumlah_bm1')">
                            <span class="input-group-addon">ls</span>
                          </div>
                        </div>
                        <p class="col-sm-4" id="harga_bm1"></p>
                        <p class="col-sm-4" id="jumlah_bm1">Rp 0,00</p>
                      </div>
                      <br>
                      <div class="row">
                        <p class="col-sm-12">Belanja Bahan Percontohan Pelatihan</p>
                      </div>
                      <div class="row">
                        <div class="col-sm-4">
                          <div class="input-group">
                            <input type="text" name="vol_bm2" id="vol_bm2" class="form-control" placeholder="volume" onkeyup="totalHarga('vol_bm2','harga_bm2','jumlah_bm2')">
                            <span class="input-group-addon">ls</span>
                          </div>
                        </div>
                        <p class="col-sm-4" id="harga_bm2"></p>
                        <p class="col-sm-4" id="jumlah_bm2">Rp 0,00</p>
                      </div>
                      <br>
                      <div class="row">
                        <p class="col-sm-12">Belanja Alat-alat/Perlengkapan Pelatihan</p>
                      </div>
                      <div class="row">
                        <div class="col-sm-4">
                          <div class="input-group">
                            <input type="text" name="vol_bm3" id="vol_bm3" class="form-control" placeholder="volume" onkeyup="totalHarga('vol_bm3','harga_bm3','jumlah_bm3')">
                            <span class="input-group-addon">ls</span>
                          </div>
                        </div>
                        <p class="col-sm-4" id="harga_bm3">Rp 220.000,00</p>
                        <p class="col-sm-4" id="jumlah_bm3">Rp 0,00</p>
                      </div>
                    </div>
                  </div><!-- /.form group -->
                  <hr>
                  <div class="form-group">
                    <div class="col-sm-12">
                      <div class="row">
                        <label class="col-sm-12">Belanja Jasa Kantor</label>
                      </div>
                      <div class="row">
                        <p class="col-sm-12">Belanja Dekorasi Pelatihan</p>
                      </div>
                      <div class="row">
                        <div class="col-sm-4">
                          <div class="input-group">
                            <input type="text" name="vol_jk1" id="vol_jk1" class="form-control" placeholder="volume" onkeyup="totalHarga('vol_jk1','harga_jk1','jumlah_jk1')">
                            <span class="input-group-addon">kgt</span>
                          </div>
                        </div>
                        <p class="col-sm-4" id="harga_jk1">Rp 250.000,00</p>
                        <p class="col-sm-4" id="jumlah_jk1">Rp 0,00</p>
                      </div>
                      <br>
                      <div class="row">
                        <p class="col-sm-12">Belanja Dekorasi Praktek Lapangan</p>
                      </div>
                      <div class="row">
                        <div class="col-sm-4">
                          <div class="input-group">
                            <input type="text" name="vol_jk2" id="vol_jk2" class="form-control" placeholder="volume" onkeyup="totalHarga('vol_jk2','harga_jk2','jumlah_jk2')">
                            <span class="input-group-addon">kgt</span>
                          </div>
                        </div>
                        <p class="col-sm-4" id="harga_jk2">Rp 250.000,00</p>
                        <p class="col-sm-4" id="jumlah_jk2">Rp 0,00</p>
                      </div>
                    </div>
                  </div><!-- /.form group -->
                  <hr>
                  <div class="form-group">
                    <div class="col-sm-12">
                      <div class="row">
                        <label class="col-sm-12">Belanja Cetak dan Penggadaan</label>
                      </div>
                      <div class="row">
                        <p class="col-sm-12">Penggandaan</p>
                      </div>
                      <div class="row">
                        <div class="col-sm-4">
                          <div class="input-group">
                            <input type="text" name="vol_cp1" id="vol_cp1" class="form-control" placeholder="volume" onkeyup="totalHarga('vol_cp1','harga_cp1','jumlah_cp1')">
                            <span class="input-group-addon">lbr</span>
                          </div>
                        </div>
                        <p class="col-sm-4" id="harga_cp1">Rp 125,00</p>
                        <p class="col-sm-4" id="jumlah_cp1">Rp 0,00</p>
                      </div>
                    </div>
                  </div><!-- /.form group -->
                  <hr>
                  <div class="form-group">
                    <div class="col-sm-12">
                      <div class="row">
                        <label class="col-sm-12">Belanja Sewa Rumah/ Gedung/Gudang/ Parkir</label>
                      </div>
                      <div class="row">
                        <p class="col-sm-12">Belanja Sewa Tempat Pelatihan</p>
                      </div>
                      <div class="row">
                        <div class="col-sm-4">
                          <div class="input-group">
                            <input type="text" name="vol_stp1" id="vol_stp1" class="form-control" placeholder="volume" onkeyup="totalHarga('vol_stp1','harga_stp1','jumlah_stp1')">
                            <span class="input-group-addon">kgt</span>
                          </div>
                        </div>
                        <p class="col-sm-4" id="harga_stp1">Rp 500.000,00</p>
                        <p class="col-sm-4" id="jumlah_stp1">Rp 0,00</p>
                      </div>
                    </div>
                  </div><!-- /.form group -->
                  <hr>
                  <div class="form-group">
                    <div class="col-sm-12">
                      <div class="row">
                        <label class="col-sm-12">Belanja Sewa kamar Hotel/Penginapan</label>
                      </div>
                      <div class="row">
                        <p class="col-sm-12">Belanja Sewa Penginapan Praktek lapangan</p>
                      </div>
                      <div class="row">
                        <div class="col-sm-4">
                          <div class="input-group">
                            <input type="text" name="vol_sp1" id="vol_sp1" class="form-control" placeholder="volume" onkeyup="totalHarga('vol_sp1','harga_sp1','jumlah_sp1')">
                            <span class="input-group-addon">OH</span>
                          </div>
                        </div>
                        <p class="col-sm-4" id="harga_sp1">Rp 500.000,00</p>
                        <p class="col-sm-4" id="jumlah_sp1">Rp 0,00</p>
                      </div>
                    </div>
                  </div><!-- /.form group -->
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <div class="col-sm-12">
                      <div class="row">
                        <label class="col-sm-12">Belanja Sewa Sarana Mobilitas</label>
                      </div>
                      <div class="row">
                        <p class="col-sm-12">Belanja Sarana Mobilitas Darat Survey Praktek Lapangan</p>
                      </div>
                      <div class="row">
                        <div class="col-sm-4">
                          <div class="input-group">
                            <input type="text" name="vol_ss1" id="vol_ss1" class="form-control" placeholder="volume" onkeyup="totalHarga('vol_ss1','harga_ss1','jumlah_ss1')">
                            <span class="input-group-addon">Hari</span>
                          </div>
                        </div>
                        <p class="col-sm-4" id="harga_ss1"></p>
                        <p class="col-sm-4" id="jumlah_ss1">Rp 0,00</p>
                      </div>
                      <br>
                      <div class="row">
                        <p class="col-sm-12">Belanja Sarana Mobilitas Darat Praktek Lapangan</p>
                      </div>
                      <div class="row">
                        <div class="col-sm-4">
                          <div class="input-group">
                            <input type="text" name="vol_ss2" id="vol_ss2" class="form-control" placeholder="volume" onkeyup="totalHarga('vol_ss2','harga_ss2','jumlah_ss2')">
                            <span class="input-group-addon">Hari</span>
                          </div>
                        </div>
                        <p class="col-sm-4" id="harga_ss2"></p>
                        <p class="col-sm-4" id="jumlah_ss2">Rp 0,00</p>
                      </div>
                    </div>
                  </div><!-- /.form group -->
                  <hr>
                  <div class="form-group">
                    <div class="col-sm-12">
                      <div class="row">
                        <label class="col-sm-12">Biaya Makan dan Minum</label>
                      </div>
                      <div class="row">
                        <p class="col-sm-12">Belanja  Snack  dan Minum Rapat </p>
                      </div>
                      <div class="row">
                        <div class="col-sm-4">
                          <div class="input-group">
                            <input type="text" name="vol_mm1" id="vol_mm1" class="form-control" placeholder="volume" onkeyup="totalHarga('vol_mm1','harga_mm1','jumlah_mm1')">
                            <span class="input-group-addon">OH</span>
                          </div>
                        </div>
                        <p class="col-sm-4" id="harga_mm1">Rp 7.500,00</p>
                        <p class="col-sm-4" id="jumlah_mm1">Rp 0,00</p>
                      </div>
                      <br>
                      <div class="row">
                        <p class="col-sm-12">Belanja  Snack  dan Minum Kegiatan Pelatihan</p>
                      </div>
                      <div class="row">
                        <div class="col-sm-4">
                          <div class="input-group">
                            <input type="text" name="vol_mm2" id="vol_mm2" class="form-control" placeholder="volume" onkeyup="totalHarga('vol_mm2','harga_mm2','jumlah_mm2')">
                            <span class="input-group-addon">OH</span>
                          </div>
                        </div>
                        <p class="col-sm-4" id="harga_mm2">Rp 8.000,00</p>
                        <p class="col-sm-4" id="jumlah_mm2">Rp 0,00</p>
                      </div>
                      <br>
                      <div class="row">
                        <p class="col-sm-12">Belanja Makan dan Minum Kegiatan Pelatihan</p>
                      </div>
                      <div class="row">
                        <div class="col-sm-4">
                          <div class="input-group">
                            <input type="text" name="vol_mm3" id="vol_mm3" class="form-control" placeholder="volume" onkeyup="totalHarga('vol_mm3','harga_mm3','jumlah_mm3')">
                            <span class="input-group-addon">OH</span>
                          </div>
                        </div>
                        <p class="col-sm-4" id="harga_mm3">Rp 17.500,00</p>
                        <p class="col-sm-4" id="jumlah_mm3">Rp 0,00</p>
                      </div>
                      <br>
                      <div class="row">
                        <p class="col-sm-12">Belanja Snack dan Minum (8x) Praktek Lapangan</p>
                      </div>
                      <div class="row">
                        <div class="col-sm-4">
                          <div class="input-group">
                            <input type="text" name="vol_mm4" id="vol_mm4" class="form-control" placeholder="volume" onkeyup="totalHarga('vol_mm4','harga_mm4','jumlah_mm4')">
                            <span class="input-group-addon">OH</span>
                          </div>
                        </div>
                        <p class="col-sm-4" id="harga_mm4">Rp 8.500,00</p>
                        <p class="col-sm-4" id="jumlah_mm4">Rp 0,00</p>
                      </div>
                      <br>
                      <div class="row">
                        <p class="col-sm-12">Belanja Makanan dan Minum (8x) Praktek Lapangan</p>
                      </div>
                      <div class="row">
                        <div class="col-sm-4">
                          <div class="input-group">
                            <input type="text" name="vol_mm5" id="vol_mm5" class="form-control" placeholder="volume" onkeyup="totalHarga('vol_mm5','harga_mm5','jumlah_mm5')">
                            <span class="input-group-addon">OH</span>
                          </div>
                        </div>
                        <p class="col-sm-4" id="harga_mm5">Rp 28.000,00</p>
                        <p class="col-sm-4" id="jumlah_mm5">Rp 0,00</p>
                      </div>
                    </div>
                  </div><!-- /.form group -->
                  <hr>
                  <div class="form-group">
                    <div class="col-sm-12">
                      <div class="row">
                        <label class="col-sm-12">Belanja Perjalanan ke Dalam Daerah </label>
                      </div>
                      <div class="row">
                        <p class="col-sm-12">Gol IV (Dalam Provinsi) </p>
                      </div>
                      <div class="row">
                        <div class="col-sm-4">
                          <div class="input-group">
                            <input type="text" name="vol_pdd1" id="vol_pdd1" class="form-control" placeholder="volume" onkeyup="totalHarga('vol_pdd1','harga_pdd1','jumlah_pdd1')">
                            <span class="input-group-addon">OH</span>
                          </div>
                        </div>
                        <p class="col-sm-4" id="harga_pdd1">Rp 70.000,00</p>
                        <p class="col-sm-4" id="jumlah_pdd1">Rp 0,00</p>
                      </div>
                      <br>
                      <div class="row">
                        <p class="col-sm-12">Gol III (Dalam Provinsi)</p>
                      </div>
                      <div class="row">
                        <div class="col-sm-4">
                          <div class="input-group">
                            <input type="text" name="vol_pdd2" id="vol_pdd2" class="form-control" placeholder="volume" onkeyup="totalHarga('vol_pdd2','harga_pdd2','jumlah_pdd2')">
                            <span class="input-group-addon">OH</span>
                          </div>
                        </div>
                        <p class="col-sm-4" id="harga_pdd2">Rp 60.000,00</p>
                        <p class="col-sm-4" id="jumlah_pdd2">Rp 0,00</p>
                      </div>
                      <br>
                      <div class="row">
                        <p class="col-sm-12">Gol III (Dalam Kabupaten)</p>
                      </div>
                      <div class="row">
                        <div class="col-sm-4">
                          <div class="input-group">
                            <input type="text" name="vol_pdd3" id="vol_pdd3" class="form-control" placeholder="volume" onkeyup="totalHarga('vol_pdd3','harga_pdd3','jumlah_pdd3')">
                            <span class="input-group-addon">OH</span>
                          </div>
                        </div>
                        <p class="col-sm-4" id="harga_pdd3">Rp 40.000,00</p>
                        <p class="col-sm-4" id="jumlah_pdd3">Rp 0,00</p>
                      </div>
                      <br>
                      <div class="row">
                        <p class="col-sm-12">Gol II (Dalam Kabupaten)</p>
                      </div>
                      <div class="row">
                        <div class="col-sm-4">
                          <div class="input-group">
                            <input type="text" name="vol_pdd4" id="vol_pdd4" class="form-control" placeholder="volume" onkeyup="totalHarga('vol_pdd4','harga_pdd4','jumlah_pdd4')">
                            <span class="input-group-addon">OH</span>
                          </div>
                        </div>
                        <p class="col-sm-4" id="harga_pdd4">Rp 30.000,00</p>
                        <p class="col-sm-4" id="jumlah_pdd4">Rp 0,00</p>
                      </div>
                    </div>
                  </div><!-- /.form group -->
                  <hr>
                  <div class="form-group">
                    <div class="col-sm-12">
                      <div class="row">
                        <label class="col-sm-12">Belanja Perjalanan ke Luar Daerah</label>
                      </div>
                      <div class="row">
                        <p class="col-sm-12">Gol III : Lumpsum : 3 org x 2 hr x 3 kali</p>
                      </div>
                      <div class="row">
                        <div class="col-sm-4">
                          <div class="input-group">
                            <input type="text" name="vol_pld1" id="vol_pld1" class="form-control" placeholder="volume" onkeyup="totalHarga('vol_pld1','harga_pld1','jumlah_pld1')">
                            <span class="input-group-addon">OH</span>
                          </div>
                        </div>
                        <p class="col-sm-4" id="harga_pld1">Rp 1.070.000,00</p>
                        <p class="col-sm-4" id="jumlah_pld1">Rp 0,00</p>
                      </div>
                      <br>
                      <div class="row">
                        <p class="col-sm-12">Lumpsum 3 hari x 10 pendamping Praktek Lapangan X 3 kali</p>
                      </div>
                      <div class="row">
                        <div class="col-sm-4">
                          <div class="input-group">
                            <input type="text" name="vol_pld2" id="vol_pld2" class="form-control" placeholder="volume" onkeyup="totalHarga('vol_pld2','harga_pld2','jumlah_pld2')">
                            <span class="input-group-addon">OH</span>
                          </div>
                        </div>
                        <p class="col-sm-4" id="harga_pld2">Rp 200.000,00</p>
                        <p class="col-sm-4" id="jumlah_pld2">Rp 0,00</p>
                      </div>
                    </div>
                  </div><!-- /.form group -->
                  <hr>
                  <div class="form-group">
                    <div class="col-sm-12">
                      <div class="row">
                        <label class="col-sm-12">Honorarium Pelaksanaan Kegiatan</label>
                      </div>
                      <div class="row">
                        <p class="col-sm-12">Honor Instruktur Pelatihan </p>
                      </div>
                      <div class="row">
                        <div class="col-sm-4">
                          <div class="input-group">
                            <input type="text" name="vol_honor_pk1" id="vol_honor_pk1" class="form-control" placeholder="volume" onkeyup="totalHarga('vol_honor_pk1','harga_honor_pk1','jumlah_honor_pk1')">
                            <span class="input-group-addon">JPL</span>
                          </div>
                        </div>
                        <p class="col-sm-4" id="harga_honor_pk1">Rp 250.000,00</p>
                        <p class="col-sm-4" id="jumlah_honor_pk1">Rp 0,00</p>
                      </div>
                      <br>
                      <div class="row">
                        <p class="col-sm-12">Honor Instruktur Praktek Lapangan</p>
                      </div>
                      <div class="row">
                        <div class="col-sm-4">
                          <div class="input-group">
                            <input type="text" name="vol_honor_pk2" id="vol_honor_pk2" class="form-control" placeholder="volume" onkeyup="totalHarga('vol_honor_pk2','harga_honor_pk2','jumlah_hono_pk2')">
                            <span class="input-group-addon">JPL</span>
                          </div>
                        </div>
                        <p class="col-sm-4" id="harga_honor_pk2">Rp 250.000,00</p>
                        <p class="col-sm-4" id="jumlah_hono_pk2">Rp 0,00</p>
                      </div>
                      <br>
                      <div class="row">
                        <p class="col-sm-12">Uang Saku Peserta Praktek Lapangan </p>
                      </div>
                      <div class="row">
                        <div class="col-sm-4">
                          <div class="input-group">
                            <input type="text" name="vol_honor_pk3" id="vol_honor_pk3" class="form-control" placeholder="volume" onkeyup="totalHarga('vol_honor_pk3','harga_honor_pk3','jumlah_hono_pk3')">
                            <span class="input-group-addon">OH</span>
                          </div>
                        </div>
                        <p class="col-sm-4" id="harga_honor_pk3">Rp 65.000,00</p>
                        <p class="col-sm-4" id="jumlah_hono_pk3">Rp 0,00</p>
                      </div>
                    </div>
                  </div><!-- /.form group -->
                </div>
              </div>
              <hr>
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <div class="col-sm-12">
                      <div class="row">
                         <label class="col-sm-12">Keterangan</label>
                      </div>
                      <div class="row">
                        <div class="col-sm-12">
                          <div class=" input-group">
                            <div class="input-group-addon">
                              <i class="fa fa-file-text"></i>
                            </div>
                            <input type="text" name="keterangan" class="form-control">
                          </div>
                        </div><!-- /.input group -->
                      </div>
                      <br>
                      <div class="row">
                         <label class="col-sm-12">Tanggal Pembahasan</label>
                      </div>
                      <div class="row">
                        <div class="col-sm-12">
                          <div class=" input-group">
                            <div class="input-group-addon">
                              <i class="fa fa-calendar"></i>
                            </div>
                            <input type="text" name="tgl_pembahasan" class="form-control pull-right" id="tgl_pembahasan" value="{{date('d-m-Y')}}">
                          </div>
                        </div><!-- /.input group -->
                      </div>
                      <br>
                      <div class="row">
                         <label class="col-sm-12">Catatan Hasil Pembahasan</label>
                      </div>
                      <div class="row">
                        <div class="col-sm-12">
                          <div class=" input-group">
                            <span class="input-group-addon">1</span>
                            <input type="text" name="catatan_pembahasan1" class="form-control pull-right" id="catatan_pembahasan">
                          </div>
                          <div class=" input-group">
                            <span class="input-group-addon">2</span>
                            <input type="text" name="catatan_pembahasan2" class="form-control pull-right" id="catatan_pembahasan">
                          </div>
                          <div class=" input-group">
                            <span class="input-group-addon">3</span>
                            <input type="text" name="catatan_pembahasan3" class="form-control pull-right" id="catatan_pembahasan">
                          </div>
                        </div><!-- /.input group -->
                      </div>
                    </div>
                  </div><!-- /.form group -->
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <div class="col-sm-12">
                      <div class="row">
                         <label class="col-sm-12">Tim Anggaran Pemerintah Daerah</label>
                      </div>
                      <div class="row">
                        <p class="col-sm-12" >Kepala Bappeda</p>
                      </div>
                      <div class="row">
                        <div class="col-sm-8">
                          <div class=" input-group">
                            <span class="input-group-addon">1</span>
                            <input type="text" name="tim_anggaran1" class="form-control" placeholder="Nama" >
                          </div>
                        </div>
                        <div class="col-sm-4">
                          <input type="text" name="nim_anggaran1" class="form-control" placeholder="NIP" >
                        </div>
                      </div>
                      <br>
                      <div class="row">
                        <p class="col-sm-12" >Kabag Adm.Pembangunan</p>
                      </div>
                      <div class="row">
                        <div class="col-sm-8">
                          <div class=" input-group">
                            <span class="input-group-addon">2</span>
                            <input type="text" name="tim_anggaran2" class="form-control" placeholder="Nama" >
                          </div>
                        </div>
                        <div class="col-sm-4">
                          <input type="text" name="nim_anggaran2" class="form-control" placeholder="NIP" >
                        </div>
                      </div>
                      <br>
                      <div class="row">
                        <p class="col-sm-12" >Kepala DPKAD</p>
                      </div>
                      <div class="row">
                        <div class="col-sm-8">
                          <div class=" input-group">
                            <span class="input-group-addon">3</span>
                            <input type="text" name="tim_anggaran3" class="form-control" placeholder="Nama" >
                          </div>
                        </div>
                        <div class="col-sm-4">
                          <input type="text" name="nim_anggaran3" class="form-control" placeholder="NIP" >
                        </div>
                      </div>
                    </div>
                  </div><!-- /.form group-->
                </div>
              </div>
              <!-- text input -->
            </div><!-- /.box-body -->
            <div class="box-footer">
              <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
              <button type="reset" class="btn btn-warning">Reset</button>
              <button type="submit" class="btn btn-primary">Simpan</button>
            </div>
          </form>
        </div><!-- /.box -->
      </div><!--/.col (left) -->
    </div>   <!-- /.row -->
  </section><!-- /.content -->
</div><!-- /.content-wrapper -->
@stop
@section('script')
<script>
  $(function () {
    //Initialize Select2 Elements
    $(".select2").select2();

    //Datemask dd/mm/yyyy
    $("#datemask").inputmask("dd/mm/yyyy", {"placeholder": "dd/mm/yyyy"});
    //Datemask2 mm/dd/yyyy
    $("#datemask2").inputmask("mm/dd/yyyy", {"placeholder": "mm/dd/yyyy"});
    //Money Euro
    $("[data-mask]").inputmask();

    //Date range picker
    //$('#reservation').daterangepicker();
    $('#reservation').datepicker({
      maxViewMode:2,
      minViewMode:2,
      format:"yyyy"
    });
    $('#tgl_pembahasan').datepicker({
      format:"dd-mm-yyyy",
      todayHighlight:true
    });
    //Date range picker with time picker
    $('#reservationtime').daterangepicker({timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A'});
    //Date range as a button
    $('#daterange-btn').daterangepicker(
        {
          ranges: {
            'Today': [moment(), moment()],
            'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
            'This Month': [moment().startOf('month'), moment().endOf('month')],
            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
          },
          startDate: moment().subtract(29, 'days'),
          endDate: moment()
        },
    function (start, end) {
      $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
    }
    );

    //iCheck for checkbox and radio inputs
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
      checkboxClass: 'icheckbox_minimal-blue',
      radioClass: 'iradio_minimal-blue'
    });
    //Red color scheme for iCheck
    $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
      checkboxClass: 'icheckbox_minimal-red',
      radioClass: 'iradio_minimal-red'
    });
    //Flat red color scheme for iCheck
    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
      checkboxClass: 'icheckbox_flat-green',
      radioClass: 'iradio_flat-green'
    });

    //Colorpicker
    $(".my-colorpicker1").colorpicker();
    //color picker with addon
    $(".my-colorpicker2").colorpicker();

    //Timepicker
    $(".timepicker").timepicker({
      showInputs: false
    });
  });
</script>
@endsection