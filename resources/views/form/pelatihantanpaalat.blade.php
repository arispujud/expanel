@extends('master')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Pelatihan Tanpa Alat
      <small></small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="#">Forms</a></li>
      <li class="active">Pelatihan Tnapa Alat</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <!-- left column -->
      <div class="col-md-12">
        <!-- general form elements disabled -->
        <div class="panel panel-primary">
          <div class="panel-heading"><b>Form Isian</b></div>
          <form class="form-horizontal" role="form" action="{{Config::get('app.url')}}/pelatihantanpaalat" method="post">
            <div class="panel-body">
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="col-sm-3 control-label">Formulir</label>
                    <div class="col-sm-8 input-group">
                      <div class="input-group-addon">
                        <i class="fa fa-file-text"></i>
                      </div>
                      <input type="text" name="formulir" class="form-control"  value="RKA-SKPD 2.2.1">
                    </div><!-- /.input group -->
                  </div><!-- /.form group -->
                  <div class="form-group">
                    <label class="col-sm-3 control-label">Tahun Anggaran</label>
                    <div class="col-sm-8 input-group">
                      <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                      </div>
                      <input type="text" name="thn_anggaran" class="form-control pull-right" id="reservation" value="{{date('Y')}}">
                    </div><!-- /.input group -->
                  </div><!-- /.form group -->
                  <div class="form-group">
                    <label class="col-sm-3 control-label">Lokasi Kegiatan</label>
                    <div class="col-sm-8 input-group">
                      <div class="input-group-addon">
                        <i class="fa fa-map-marker"></i>
                      </div>
                      <input type="text" name="lokasi_kegiatan" class="form-control"  value="Kabupaten Bantul">
                    </div><!-- /.input group -->
                  </div><!-- /.form group -->
                  <div class="form-group">
                    <label class="col-sm-3 control-label">Sasaran Kegiatan</label>
                    <div class="col-sm-8 input-group">
                      <div class="input-group-addon">
                        <i class="fa fa-dot-circle-o"></i>
                      </div>
                      <input type="text" name="sasaran" class="form-control">
                    </div><!-- /.input group -->
                  </div><!-- /.form group -->
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="col-sm-3 control-label">Capaian Program</label>
                    <div class="col-sm-8 input-group">
                      <div class="input-group-addon">
                        <i class="fa fa-star"></i>
                      </div>
                      <input type="text" name="capaian" class="form-control">
                    </div><!-- /.input group -->
                  </div><!-- /.form group -->
                  <div class="form-group">
                    <label class="col-sm-3 control-label">Masukkan</label>
                    <div class="col-sm-8 input-group">
                      <div class="input-group-addon">
                        <i class="fa fa-download"></i>
                      </div>
                      <input type="text" name="masukkan" class="form-control">
                    </div><!-- /.input group -->
                  </div><!-- /.form group -->
                  <div class="form-group">
                    <label class="col-sm-3 control-label">Keluaran</label>
                    <div class="col-sm-8 input-group">
                      <div class="input-group-addon">
                        <i class="fa fa-upload"></i>
                      </div>
                      <input type="text" name="keluaran" class="form-control">
                    </div><!-- /.input group -->
                  </div><!-- /.form group -->
                  <div class="form-group">
                    <label class="col-sm-3 control-label">Hasil</label>
                    <div class="col-sm-8 input-group">
                      <div class="input-group-addon">
                        <i class="fa fa-thumbs-up"></i>
                      </div>
                      <input type="text" name="hasil" class="form-control">
                    </div><!-- /.input group -->
                  </div><!-- /.form group -->
                </div>
              </div>
              <hr>
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <div class="col-sm-12">
                      <div class="row">
                        <label class="col-sm-12">Rincian Anggaran Biaya Langsung</label>
                      </div>
                      <div class="row">
                        <p class="col-sm-12">Biaya ATK</p>
                      </div>
                      <div class="row">
                        <div class="col-sm-4">
                          <div class="input-group">
                            <input type="text" class="form-control" id="vol_atk" name="vol_atk" placeholder="Volume"  onkeyup="totalHarga('vol_atk','harga_atk','jumlah_atk')">
                            <span class="input-group-addon">Kegiatan</span>
                          </div>
                        </div>
                        <p class="col-sm-4" id="harga_atk">Rp 500.000,00</p>
                        <p class="col-sm-4" id="jumlah_atk">Rp 0,00</p>
                      </div>
                      <br>
                      <div class="row">
                        <p class="col-sm-12">Belanja ATK Peserta</p>
                      </div>
                      <div class="row">
                        <div class="col-sm-4">
                          <div class="input-group">
                            <input type="text" class="form-control" id="vol_bap" name="vol_bap" placeholder="Volume"  onkeyup="totalHarga('vol_bap','harga_bap','jumlah_bap')">
                            <span class="input-group-addon">Paket</span>
                          </div>
                        </div>
                        <p class="col-sm-4" id="harga_bap">Rp 65.000,00</p>
                        <p class="col-sm-4" id="jumlah_bap">Rp 0,00</p>
                      </div>
                    </div>
                  </div> 
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <div class="col-sm-12">
                      <div class="row">
                        <label class="col-sm-12">Belanja Makan dan Minuman</label>
                      </div>
                      <div class="row">
                        <p class="col-sm-12">Belanja Snack dan Minuman Kegiatan</p>
                      </div>
                      <div class="row">
                        <div class="col-sm-4">
                          <div class="input-group">
                            <input type="text" class="form-control" id="vol_bmk" name="vol_bmk" placeholder="Volume"  onkeyup="totalHarga('vol_bmk','harga_bmk','jumlah_bmk')">
                            <span class="input-group-addon">OH</span>
                          </div>
                        </div>
                        <p class="col-sm-4" id="harga_bmk">Rp 7.500,00</p>
                        <p class="col-sm-4" id="jumlah_bmk">Rp 0,00</p>
                      </div>
                      <br>
                      <div class="row">
                        <p class="col-sm-12">Belanja Makanan dan Minuman</p>
                      </div>
                      <div class="row">
                        <div class="col-sm-4">
                          <div class="input-group">
                            <input type="text" class="form-control" id="vol_mdm" name="vol_mdm" placeholder="Volume"  onkeyup="totalHarga('vol_mdm','harga_mdm','jumlah_mdm')">
                            <span class="input-group-addon">OH</span>
                          </div>
                        </div>
                        <p class="col-sm-4" id="harga_mdm">Rp 17.500,00</p>
                        <p class="col-sm-4" id="jumlah_mdm">Rp 0,00</p>
                      </div>
                    </div>
                  </div>
                </div>      
              </div>
              <hr>
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <div class="col-sm-12">
                      <div class="row">
                        <label class="col-sm-12">Belanja Bahan Material</label>
                      </div>
                      <div class="row">
                        <p class="col-sm-12">Belanja Bahan Praktek</p>
                      </div>
                      <div class="row">
                        <div class="col-sm-4">
                          <div class="input-group">
                            <input type="text" class="form-control" id="vol_bbp" name="vol_bbp" placeholder="Volume"  onkeyup="totalHarga('vol_bbp','harga_bbp','jumlah_bbp')">
                            <span class="input-group-addon">LS</span>
                          </div>
                        </div>
                        <p class="col-sm-4" id="harga_bbp">Rp 00,00</p>
                        <p class="col-sm-4" id="jumlah_bbp">Rp 0,00</p>
                      </div>
                      <br>
                      <div class="row">
                        <label class="col-sm-12">Belanja Jasa Kantor</label>
                      </div>
                      <div class="row">
                        <p class="col-sm-12">Belanja Dekorasi</p>
                      </div>
                      <div class="row">
                        <div class="col-sm-4">
                          <div class="input-group">
                            <input type="text" class="form-control" id="vol_bd" name="vol_bd" placeholder="Volume"  onkeyup="totalHarga('vol_bd','harga_bd','jumlah_bd')">
                            <span class="input-group-addon">KGT</span>
                          </div>
                        </div>
                        <p class="col-sm-4" id="harga_bd">Rp 300.000,00</p>
                        <p class="col-sm-4" id="jumlah_bd">Rp 0,00</p>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <div class="col-sm-12">
                        <div class="row">
                          <label class="col-sm-12">Belanja Cetak Pengadaan</label>
                        </div>
                        <div class="row">
                          <p class="col-sm-12">Pengadaan</p>
                        </div>
                        <div class="row">
                          <div class="col-sm-4">
                            <div class="input-group">
                              <input type="text" class="form-control" id="vol_bcp" name="vol_bcp" placeholder="Volume"  onkeyup="totalHarga('vol_bcp','harga_bcp','jumlah_bcp')">
                              <span class="input-group-addon">lbr</span>
                            </div>
                          </div>
                          <p class="col-sm-4" id="harga_bcp">Rp 125,00</p>
                          <p class="col-sm-4" id="jumlah_bcp">Rp 0,00</p>
                        </div>
                        <br>
                        <div class="row">
                          <label class="col-sm-12">Biaya Sewa Rumah dan Gedung</label>
                        </div>
                        <div class="row">
                          <p class="col-sm-12">Sewa Tempat Kegiatan</p>
                        </div>
                        <div class="row">
                          <div class="col-sm-4">
                            <div class="input-group">
                              <input type="text" class="form-control" id="vol_stk" name="vol_stk" placeholder="Volume"  onkeyup="totalHarga('vol_stk','harga_stk','jumlah_stk')">
                              <span class="input-group-addon">Hari</span>
                            </div>
                          </div>
                          <p class="col-sm-4" id="harga_stk">Rp 500.000,00</p>
                          <p class="col-sm-4" id="jumlah_stk">Rp 0,00</p>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <hr>
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <div class="col-sm-12">
                      <div class="row">
                        <label class="col-sm-12">Belanja Perjalanan Dinas</label>
                      </div>
                      <div class="row">
                        <p class="col-sm-12">Gol II</p>
                      </div>
                      <div class="row">
                        <div class="col-sm-4">
                          <div class="input-group">
                            <input type="text" class="form-control" id="vol_gol2" name="vol_gol2" placeholder="Volume"  onkeyup="totalHarga('vol_gol2','harga_gol2','jumlah_gol2')">
                            <span class="input-group-addon">OH</span>
                          </div>
                        </div>
                        <p class="col-sm-4" id="harga_gol2">Rp 40.000,00</p>
                        <p class="col-sm-4" id="jumlah_gol2">Rp 0,00</p>
                      </div>
                      <br>
                      <div class="row">
                        <p class="col-sm-12">Gol III</p>
                      </div>
                      <div class="row">
                        <div class="col-sm-4">
                          <div class="input-group">
                            <input type="text" class="form-control" id="vol_gol3" name="vol_gol3" placeholder="Volume"  onkeyup="totalHarga('vol_gol3','harga_gol3','jumlah_gol3')">
                            <span class="input-group-addon">OH</span>
                          </div>
                        </div>
                        <p class="col-sm-4" id="harga_gol3">Rp 50.000,00</p>
                        <p class="col-sm-4" id="jumlah_gol3">Rp 0,00</p>
                      </div>
                      <br>
                      <div class="row">
                        <p class="col-sm-12">Gol IV</p>
                      </div>
                      <div class="row">
                        <div class="col-sm-4">
                          <div class="input-group">
                            <input type="text" class="form-control" id="vol_gol4" name="vol_gol4" placeholder="Volume"  onkeyup="totalHarga('vol_gol4','harga_gol4','jumlah_gol4')">
                            <span class="input-group-addon">OH</span>
                          </div>
                        </div>
                        <p class="col-sm-4" id="harga_gol4">Rp 60.000,00</p>
                        <p class="col-sm-4" id="jumlah_gol4">Rp 0,00</p>
                      </div>
                    </div>
                  </div> 
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <div class="col-sm-12">
                      <div class="row">
                        <label class="col-sm-12">Honorium Kegiatan</label>
                      </div>
                      <div class="row">
                        <p class="col-sm-12">HR. Instruktur</p>
                      </div>
                      <div class="row">
                        <div class="col-sm-4">
                          <div class="input-group">
                            <input type="text" class="form-control" id="vol_hr" name="vol_hr" placeholder="Volume"  
                            onkeyup="totalHarga('vol_hr','harga_hr','jumlah_hr')">
                            <span class="input-group-addon">JPL</span>
                          </div>
                        </div>
                        <p class="col-sm-4" id="harga_hr">Rp 250.000,00</p>
                        <p class="col-sm-4" id="jumlah_hr">Rp 0,00</p>
                      </div>
                      <br>
                      <div class="row">
                        <label class="col-sm-12">Honor Peserta Seminar/Sosialisasi/Workshop</label>
                      </div>
                      <div class="row">
                        <p class="col-sm-12">Honor Peserta</p>
                      </div>
                      <div class="row">
                        <div class="col-sm-4">
                          <div class="input-group">
                            <input type="text" class="form-control" id="vol_hps" name="vol_hps" placeholder="Volume"  onkeyup="totalHarga('vol_hps','harga_hps','jumlah_hps')">
                            <span class="input-group-addon">OH</span>
                          </div>
                        </div>
                        <p class="col-sm-4" id="harga_hps">Rp 30.000,00</p>
                        <p class="col-sm-4" id="jumlah_hps">Rp 0,00</p>
                      </div>
                      <br>
                    </div>
                  </div>
                </div>      
              </div>
              <hr>
                            <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <div class="col-sm-12">
                      <div class="row">
                         <label class="col-sm-12">Keterangan</label>
                      </div>
                      <div class="row">
                        <div class="col-sm-12">
                          <div class=" input-group">
                            <div class="input-group-addon">
                              <i class="fa fa-file-text"></i>
                            </div>
                            <input type="text" name="keterangan" class="form-control">
                          </div>
                        </div><!-- /.input group -->
                      </div>
                      <br>
                      <div class="row">
                         <label class="col-sm-12">Tanggal Pembahasan</label>
                      </div>
                      <div class="row">
                        <div class="col-sm-12">
                          <div class=" input-group">
                            <div class="input-group-addon">
                              <i class="fa fa-calendar"></i>
                            </div>
                            <input type="text" name="tgl_pembahasan" class="form-control pull-right" id="tgl_pembahasan" value="{{date('d-m-Y')}}">
                          </div>
                        </div><!-- /.input group -->
                      </div>
                      <br>
                      <div class="row">
                         <label class="col-sm-12">Catatan Hasil Pembahasan</label>
                      </div>
                      <div class="row">
                        <div class="col-sm-12">
                          <div class=" input-group">
                            <span class="input-group-addon">1</span>
                            <input type="text" name="catatan_pembahasan1" class="form-control pull-right" id="catatan_pembahasan">
                          </div>
                          <div class=" input-group">
                            <span class="input-group-addon">2</span>
                            <input type="text" name="catatan_pembahasan2" class="form-control pull-right" id="catatan_pembahasan">
                          </div>
                          <div class=" input-group">
                            <span class="input-group-addon">3</span>
                            <input type="text" name="catatan_pembahasan3" class="form-control pull-right" id="catatan_pembahasan">
                          </div>
                        </div><!-- /.input group -->
                      </div>
                    </div>
                  </div><!-- /.form group -->
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <div class="col-sm-12">
                      <div class="row">
                         <label class="col-sm-12">Tim Anggaran Pemerintah Daerah</label>
                      </div>
                      <div class="row">
                        <p class="col-sm-12" >Kepala Bappeda</p>
                      </div>
                      <div class="row">
                        <div class="col-sm-8">
                          <div class=" input-group">
                            <span class="input-group-addon">1</span>
                            <input type="text" name="tim_anggaran1" class="form-control" placeholder="Nama" >
                          </div>
                        </div>
                        <div class="col-sm-4">
                          <input type="text" name="nim_anggaran1" class="form-control" placeholder="NIP" >
                        </div>
                      </div>
                      <br>
                      <div class="row">
                        <p class="col-sm-12" >Kabag Adm.Pembangunan</p>
                      </div>
                      <div class="row">
                        <div class="col-sm-8">
                          <div class=" input-group">
                            <span class="input-group-addon">2</span>
                            <input type="text" name="tim_anggaran2" class="form-control" placeholder="Nama" >
                          </div>
                        </div>
                        <div class="col-sm-4">
                          <input type="text" name="nim_anggaran2" class="form-control" placeholder="NIP" >
                        </div>
                      </div>
                      <br>
                      <div class="row">
                        <p class="col-sm-12" >Kepala DPKAD</p>
                      </div>
                      <div class="row">
                        <div class="col-sm-8">
                          <div class=" input-group">
                            <span class="input-group-addon">3</span>
                            <input type="text" name="tim_anggaran3" class="form-control" placeholder="Nama" >
                          </div>
                        </div>
                        <div class="col-sm-4">
                          <input type="text" name="nim_anggaran3" class="form-control" placeholder="NIP" >
                        </div>
                      </div>
                    </div>
                  </div><!-- /.form group-->
                </div>
              </div>
              <div class="box-footer">
                <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
                <button type="reset" class="btn btn-warning">Reset</button>
                <button type="submit" class="btn btn-primary">Simpan</butto>
              </div>
            </form>
          </div><!-- /.box-body -->
        </div><!-- /.box -->
      </div><!--/.col (right) -->
    </div>   <!-- /.row -->
  </section><!-- /.content -->
</div><!-- /.content-wrapper -->
@stop
@section('script')
<script>
  $(function () {
    //Initialize Select2 Elements
    $(".select2").select2();

    //Datemask dd/mm/yyyy
    $("#datemask").inputmask("dd/mm/yyyy", {"placeholder": "dd/mm/yyyy"});
    //Datemask2 mm/dd/yyyy
    $("#datemask2").inputmask("mm/dd/yyyy", {"placeholder": "mm/dd/yyyy"});
    //Money Euro
    $("[data-mask]").inputmask();

    //Date range picker
    //$('#reservation').daterangepicker();
    $('#reservation').datepicker({
      maxViewMode:2,
      minViewMode:2,
      format:"yyyy"
    });
    $('#tgl_pembahasan').datepicker({
      format:"dd-mm-yyyy",
      todayHighlight:true
    });
    //Date range picker with time picker
    $('#reservationtime').daterangepicker({timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A'});
    //Date range as a button
    $('#daterange-btn').daterangepicker(
        {
          ranges: {
            'Today': [moment(), moment()],
            'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
            'This Month': [moment().startOf('month'), moment().endOf('month')],
            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
          },
          startDate: moment().subtract(29, 'days'),
          endDate: moment()
        },
    function (start, end) {
      $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
    }
    );

    //iCheck for checkbox and radio inputs
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
      checkboxClass: 'icheckbox_minimal-blue',
      radioClass: 'iradio_minimal-blue'
    });
    //Red color scheme for iCheck
    $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
      checkboxClass: 'icheckbox_minimal-red',
      radioClass: 'iradio_minimal-red'
    });
    //Flat red color scheme for iCheck
    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
      checkboxClass: 'icheckbox_flat-green',
      radioClass: 'iradio_flat-green'
    });

    //Colorpicker
    $(".my-colorpicker1").colorpicker();
    //color picker with addon
    $(".my-colorpicker2").colorpicker();

    //Timepicker
    $(".timepicker").timepicker({
      showInputs: false
    });
  });
</script>
@endsection