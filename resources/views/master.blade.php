<?php
   $session   = Session::get('expanel');

  // if(!isset($session['username'])){
  //   //echo $session['username']; exit();
  //   //header("location : {{Config::get('app.url')}}/login");
  //   redirect('/login');
  // }
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>{{Config::get('app.name')}} | {{$data}}</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="{{Config::get('app.url')}}/public/assets/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{Config::get('app.url')}}/public/assets/dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="{{Config::get('app.url')}}/public/assets/dist/css/skins/_all-skins.min.css">
    <!-- iCheck for checkboxes and radio inputs -->
    <link rel="stylesheet" href="{{Config::get('app.url')}}/public/assets/plugins/iCheck/all.css">
    <!-- Morris chart -->
    <link rel="stylesheet" href="{{Config::get('app.url')}}/public/assets/plugins/morris/morris.css">
    <!-- jvectormap -->
    <link rel="stylesheet" href="{{Config::get('app.url')}}/public/assets/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
    <!-- Date Picker -->
    <link rel="stylesheet" href="{{Config::get('app.url')}}/public/assets/plugins/datepicker/datepicker3.css">
    <!-- Daterange picker -->
    <link rel="stylesheet" href="{{Config::get('app.url')}}/public/assets/plugins/daterangepicker/daterangepicker-bs3.css">
    <!-- Bootstrap time Picker -->
    <link rel="stylesheet" href="{{Config::get('app.url')}}/public/assets/plugins/timepicker/bootstrap-timepicker.min.css">
    <!-- bootstrap wysihtml5 - text editor -->
    <link rel="stylesheet" href="{{Config::get('app.url')}}/public/assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="{{Config::get('app.url')}}/public/assets/dist/css/skins/_all-skins.min.css">
    <!-- Select2 -->
    <link rel="stylesheet" href="{{Config::get('app.url')}}/public/assets/plugins/select2/select2.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script src="{{Config::get('app.url')}}/public/assets/dist/js/myMath.js"></script>
  </head>
  <body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">

      <header class="main-header">
        <!-- Logo -->
        <a href="{{Config::get('app.url')}}/public/assets/index2.html" class="logo">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class="logo-mini"><b>EX</b>P</span>
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg"><b>EX</b>panel</span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">
          <!-- Sidebar toggle button-->
          <a href="{{Config::get('app.url')}}/public/assets/#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
          </a>
          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
              <!-- User Account: style can be found in dropdown.less -->
              <li class="dropdown user user-menu">
                <a href="{{Config::get('app.url')}}/public/assets/#" class="dropdown-toggle" data-toggle="dropdown">
                  <img src="{{Config::get('app.url')}}/public/assets/dist/img/default_user.jpg" class="user-image" alt="User Image">
                  <span class="hidden-xs">{{$session['nama']}}</span>
                </a>
                <ul class="dropdown-menu">
                  <!-- User image -->
                  <li class="user-header">
                    <img src="{{Config::get('app.url')}}/public/assets/dist/img/default_user.jpg" class="img-circle" alt="User Image">
                    <p>
                      {{$session['nama']}}
                      <small>{{date('d-m-Y')}}</small>
                    </p>
                  </li>
                  <!-- Menu Body -->
                  <li class="user-body">
                    
                  </li>
                  <!-- Menu Footer-->
                  <li class="user-footer">
                    <!--<div class="pull-left">
                      <a href="{{Config::get('app.url')}}/public/assets/#" class="btn btn-default btn-flat">Profile</a>
                    </div>-->
                    <div class="pull-right">
                      <a href="{{Config::get('app.url')}}/logout" class="btn btn-default btn-flat">Sign out</a>
                    </div>
                  </li>
                </ul>
              </li>
            </ul>
          </div>
        </nav>
      </header>
      <!-- Left side column. contains the logo and sidebar -->
      <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- Sidebar user panel -->
          <div class="user-panel">
            <div class="pull-left image">
              <img src="{{Config::get('app.url')}}/public/assets/dist/img/default_user.jpg" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
              <p>{{$session['nama']}}</p>
              <a href="{{Config::get('app.url')}}/public/assets/#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
          </div>
          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu">
            <li class="header">MAIN NAVIGATION</li>
            <li class="@if($data=='Dashboard'){{'active'}}@endif">
              <a href="{{Config::get('app.url')}}">
                <i class="fa fa-dashboard"></i> <span>Dashboard</span>
              </a>
            </li>
            <li class="@if($data=='Pelatihan Tanpa Alat'){{'active'}}@endif">
              <a href="{{Config::get('app.url')}}/pelatihantanpaalat">
                <i class="fa fa-edit"></i> <span>Pelatihan Tanpa Alat</span>
              </a>
            </li>
            <li class="@if($data=='Pelatihan Dengan Alat'){{'active'}}@endif">
              <a href="{{Config::get('app.url')}}/pelatihandenganalat">
                <i class="fa fa-edit"></i> <span>Pelatihan Dengan Alat</span>
              </a>
            </li>
            <li class="@if($data=='Monev'){{'active'}}@endif">
              <a href="{{Config::get('app.url')}}/monev">
                <i class="fa fa-edit"></i> <span>Monev</span>
              </a>
            </li>
            <li class="@if($data=='Pameran Multi'){{'active'}}@endif">
              <a href="{{Config::get('app.url')}}/pameranmulti">
                <i class="fa fa-edit"></i> <span>Pameran Multi</span>
              </a>
            </li>
            <li class="@if($data=='Pameran'){{'active'}}@endif">
              <a href="{{Config::get('app.url')}}/pameran">
                <i class="fa fa-edit"></i> <span>Pameran</span>
              </a>
            </li>
            <li class="@if($data=='Fasilitasi'){{'active'}}@endif">
              <a href="{{Config::get('app.url')}}/fasilitasi">
                <i class="fa fa-edit"></i> <span>Fasilitasi</span>
              </a>
            </li>
            <li class="@if($data=='Manage User'){{'active'}}@endif">
              <a href="{{Config::get('app.url')}}/manageuser">
                <i class="fa fa-users"></i> <span>Manage User</span>
              </a>
            </li>
          </ul>
        </section>
        <!-- /.sidebar -->
      </aside>

      <!-- Content Wrapper. Contains page content -->
      @yield('content')
      <footer class="main-footer">
        <div class="pull-right hidden-xs">
          <b>Version</b> 2.3.0
        </div>
        <strong>Copyright &copy; 2016 <a href="http://apkit.esy.es">APKIT INNOVATION</a>.</strong> All rights reserved.
      </footer>

      
      <!-- Add the sidebar's background. This div must be placed
           immediately after the control sidebar -->
      <div class="control-sidebar-bg"></div>
    </div><!-- ./wrapper -->

    <!-- jQuery 2.1.4 -->
    <script src="{{Config::get('app.url')}}/public/assets/plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- jQuery UI 1.11.4 -->
    <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <script>
      $.widget.bridge('uibutton', $.ui.button);
    </script>
    <!-- Bootstrap 3.3.5 -->
    <script src="{{Config::get('app.url')}}/public/assets/bootstrap/js/bootstrap.min.js"></script>
    <!-- Sparkline -->
    <script src="{{Config::get('app.url')}}/public/assets/plugins/sparkline/jquery.sparkline.min.js"></script>
    <!-- jvectormap -->
    <script src="{{Config::get('app.url')}}/public/assets/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
    <script src="{{Config::get('app.url')}}/public/assets/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
    <!-- jQuery Knob Chart -->
    <script src="{{Config::get('app.url')}}/public/assets/plugins/knob/jquery.knob.js"></script>
    <!-- daterangepicker -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
    <script src="{{Config::get('app.url')}}/public/assets/plugins/daterangepicker/daterangepicker.js"></script>
    <!-- datepicker -->
    <script src="{{Config::get('app.url')}}/public/assets/plugins/datepicker/bootstrap-datepicker.js"></script>
    <!-- Bootstrap WYSIHTML5 -->
    <script src="{{Config::get('app.url')}}/public/assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
    <!-- Slimscroll -->
    <script src="{{Config::get('app.url')}}/public/assets/plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="{{Config::get('app.url')}}/public/assets/plugins/fastclick/fastclick.min.js"></script>
    <!-- AdminLTE App -->
    <script src="{{Config::get('app.url')}}/public/assets/dist/js/app.min.js"></script>
    <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
    <!--<script src="{{Config::get('app.url')}}/public/assets/dist/js/pages/dashboard.js"></script>-->
    <!-- AdminLTE for demo purposes -->
    <script src="{{Config::get('app.url')}}/public/assets/dist/js/demo.js"></script>

    <!-- Select2 -->
    <script src="{{Config::get('app.url')}}/public/assets/plugins/select2/select2.full.min.js"></script>
    <!-- InputMask -->
    <script src="{{Config::get('app.url')}}/public/assets/plugins/input-mask/jquery.inputmask.js"></script>
    <script src="{{Config::get('app.url')}}/public/assets/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
    <script src="{{Config::get('app.url')}}/public/assets/plugins/input-mask/jquery.inputmask.extensions.js"></script>
    <!-- bootstrap color picker -->
    <script src="{{Config::get('app.url')}}/public/assets/plugins/colorpicker/bootstrap-colorpicker.min.js"></script>
    <!-- bootstrap time picker -->
    <script src="{{Config::get('app.url')}}/public/assets/plugins/timepicker/bootstrap-timepicker.min.js"></script>
    <!-- SlimScroll 1.3.0 -->
    <script src="{{Config::get('app.url')}}/public/assets/plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <!-- iCheck 1.0.1 -->
    <script src="{{Config::get('app.url')}}/public/assets/plugins/iCheck/icheck.min.js"></script>
    <!-- Morris.js charts -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
    <script src="{{Config::get('app.url')}}/public/assets/plugins/morris/morris.min.js"></script>
    <!-- Page script -->
    <!--<script src="//code.jquery.com/jquery-1.12.3.js"></script>-->
    <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>
    @yield('script')
  </body>
</html>
